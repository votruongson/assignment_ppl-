/**
*Student Name: VO TRUONG SON
*Student ID  : 1512863
*/
grammar MP;

@lexer::header {
from lexererr import *
}

options{
	language=Python3;
}

program	 
		: decl+ EOF
		;



decl	 
		: vardecl
		| funcdecl
		| procdecl
		;

vardecl	 
		: VAR ( para_decl SEMI )+
		;


funcdecl 
		: FUNCTION ID LB para_list? RB COLON mptype SEMI vardecl? compound_stmt
		;

procdecl 
		: PROCEDURE ID LB para_list? RB SEMI vardecl? compound_stmt
		;

para_list
		: para_decl ( SEMI para_decl )*
		;

para_decl
		: ID (COMA ID)* COLON mptype
		;

mptype 
		: primitive_type
		| array_type
		;

primitive_type
		: booltype 
		| inttype 
		| realtype 
		| stringtype
		;
array_type
		: ARRAY LSB expr DOUBLE_DOT expr RSB OF primitive_type
		;

compound_stmt
		: BEGIN stmt* END
		;

stmt 
		: match_if
		| unmatch_if
		;

match_if
		: IF expr THEN match_if ELSE match_if
		| other
		;

unmatch_if
		: IF expr THEN stmt
		| IF expr THEN match_if ELSE unmatch_if
		| other
		;

whiledo_stmt
		: WHILE expr DO stmt
		;

for_stmt
		: FOR ID ASSIGNMENT expr ( TO | DOWNTO ) expr DO stmt
		;

with_stmt
		: WITH 	list_vardecl DO stmt
		;

other
		: assign_stmt
		| for_stmt
		| whiledo_stmt
		| break_stmt
		| continue_stmt
		| return_stmt
		| call_stmt
		| compound_stmt
		| with_stmt
		;

assign_stmt
		: ( expr5 ASSIGNMENT)+ expr SEMI
		;

break_stmt
		: BREAK SEMI
		;

continue_stmt
		: CONTINUE SEMI
		;

return_stmt
		: RETURN expr SEMI
		| RETURN SEMI
		;

list_expr
		: expr (COMA expr)*
		;


list_vardecl
		: ( para_decl SEMI )+
		;

call_stmt
		: ID LB list_expr? RB SEMI
		;

//expression

expr
		: expr1 (AND THEN | OR ELSE) expr 
		| expr1
		;


expr1
		: expr2 ( NEOP| LTOP| LEOP| GTOP| GEOP| EQOP ) expr2
		| expr2
		;

expr2
		: expr2 (ADDOP | SUBOP | OR) expr3
		| expr3
		;

expr3
		: expr3 (DIVOP | MULOP | INT_DIVOP | MODOP | AND) expr4
		| expr4
		;

expr4		
		: (NOTOP | SUBOP) expr4
		| expr5
		;
expr5	
		: expr6 LSB expr RSB 
		| expr6
		;

expr6
		: LB expr RB 
		| INTLIT
		| BOOLLIT
		| STRINGLIT
		| REALLIT
		| ID
		| function_call
		;



function_call
		: ID LB list_expr? RB
		;		



index_expr
		: expr6 LSB expr RSB 
		;

//Literal

inttype 
		: INTEGER
		;
booltype
		: BOOLEAN
		;
realtype
		: REAL
		;
stringtype
		: STRING
		;


INTLIT
		: [0-9]+
		;
REALLIT
		: REAL_NUMBER ([eE] [-]? INTLIT )?
		;
fragment REAL_NUMBER
		: (INTLIT '.')
		| ('.' INTLIT)
		| (INTLIT '.' INTLIT)
		| INTLIT
		;	
BOOLLIT
		: TRUE
		| FALSE
		;
	
		
DOUBLE_DOT
		: '..'
		;






VAR
		: [vV][aA][rR]
		;

REAL
		: [rR][eE][aA][lL]
		;

BOOLEAN
		: [bB][oO][oO][lL][eE][aA][nN]
		;

INTEGER
		: [iI][nN][tT][eE][gG][eE][rR]
		;

STRING
		: [sS][tT][rR][iI][nN][gG]
		;

ARRAY
		: [aA][rR][rR][aA][yY]
		;

BREAK
		: [bB][rR][eE][aA][kK]
		;

CONTINUE
		: [cC][oO][nN][tT][iI][nN][uU][eE]
		;

FOR
		: [fF][oO][rR]
		;

TO
		: [tT][oO]
		;
DOWNTO
		: [dD][oO][wW][nN][tT][oO]
		;
DO
		: [dD][oO]
		;

IF
		: [iI][fF]
		;

THEN
		: [tT][hH][eE][nN]
		;

ELSE
		: [eE][lL][sS][eE]
		;

RETURN
		: [rR][eE][tT][uU][rR][nN]
		;

WHILE
		: [wW][hH][iI][lL][eE]
		;

BEGIN
		: [bB][eE][gG][iI][nN]
		;

END 
		: [eE][nN][dD]
		;

FUNCTION
		: [fF][uU][nN][cC][tT][iI][oO][nN]
		;

PROCEDURE
		: [pP][rR][oO][cC][eE][dD][uU][rR][eE]
		;


OF
		: [oO][fF]
		;



NOTOP
		: [nN][oO][tT]
		;





WITH
		: [wW][iI][tT][hH]
		;

TRUE        : [tT][rR][uU][eE];
FALSE       : [fF][aA][lL][sS][eE];



ADDOP
		: '+'
		;

SUBOP
		: '-'
		;

MULOP
		: '*'
		;

DIVOP
		: '/'
		;

		
INT_DIVOP
		: [dD][iI][vV]
		;

MODOP	
		: [mM][oO][dD]
		;

AND         : [aA][nN][dD];
OR          : [oO][rR];

NEOP: '<>' ;
LTOP: '<' ;
LEOP: '<=' ;
GTOP: '>' ;
GEOP: '>=' ;
EQOP: '=' ;
ASSIGNMENT
		:':='
		;

LSB
		: '['
		;

RSB 
		: ']'
		;

COLON 
		: ':'
	  	;

LB
		: '(' 
		;

RB
		: ')' 
		;

SEMI  
		: ';' 
	  	;

COMA
		: ','
		;

ID
		: [a-zA-Z_][a-zA-Z0-9_]*
		;

WS 
		: [ \t\r\n\f]+ -> skip 
		; 



TRAINDITIONNAL_COMMENT
		:'(*' .*? '*)'  -> skip
		;

BLOCK_COMMENT
		:'{' .*? '}'	->skip
		;

LINE_COMMENT
		: '//' ~[\r\n]*	->skip
		;



//operator


TRADITIONAL_BLOCK_CMT: 	'(*' .*? '*)'	-> skip ;

BLOCK_CMT 	: 	'{' .*? '}'		-> skip ;

LINE_CMT 	: 	'//' ~[\r\n]*	-> skip ;




fragment ESCAPE
		: '\\' [bfrnt'"\\]
		;



STRINGLIT
		: '"' (ESCAPE | ~["\r\n\\] )* '"'
			{
				self.text = self.text[1:-1]
			}	
		;


UNCLOSE_STRING
		: '"' (ESCAPE | ~["\r\n])* ('\n' | EOF)
			{
				raise UncloseString(self.text[1:])
			}
		;


ILLEGAL_ESCAPE
		: '"' (ESCAPE | ~[\r\n\\])*?	([\\] ~[bfrnt'"\\])
			{
				raise IllegalEcape(self.text[1:])
			}
		(ESCAPE | ~[\r\n])* '"'
		;
ERROR_CHAR:.{raise ErrorToken(self.text)};



