import unittest
from TestUtils import TestLexer


class LexerSuite(unittest.TestCase):

    def test_1(self):
        self.assertTrue(TestLexer.test("abc","abc,<EOF>",101))
    def test_2(self):
        self.assertTrue(TestLexer.test("aCBbdc","aCBbdc,<EOF>",102))
    def test_3(self):
        self.assertTrue(TestLexer.test("aAsVN","aAsVN,<EOF>",103))
    def test_4(self):
        self.assertTrue(TestLexer.test("123a123","123,a123,<EOF>",104))
    def test_5(self):
        self.assertTrue(TestLexer.test("(abc)", "<EOF>", 105))
    def test_6(self):
        self.assertTrue(TestLexer.test("{abc}", "<EOF>", 106))
    def test_7(self):
        self.assertTrue(TestLexer.test("//abc\na", "a,<EOF>", 107))
    def test_8(self):
        self.assertTrue(TestLexer.test("//123a123123\n12345","12345,<EOF>",108))
    def test_9(self):
    	self.assertTrue(TestLexer.test("abc123", "abc123,<EOF>", 109))
    def test_10(self):
    	self.assertTrue(TestLexer.test("//abcabc", "<EOF>", 110))
    def test_11(self):
        self.assertTrue(TestLexer.test("if AND\n boOLean", "if,AND,boOLean,<EOF>", 111))
    def test_12(self):
        self.assertTrue(TestLexer.test("iF", "iF,<EOF>", 112))
    def test_13(self):
        self.assertTrue(TestLexer.test("If", "If,<EOF>", 113))
    def test_14(self):
        self.assertTrue(TestLexer.test("IF", "IF,<EOF>", 114))
    def test_15(self):
        self.assertTrue(TestLexer.test("(a123]", "(,a123,],<EOF>", 115))
    def test_16(self):
        self.assertTrue(TestLexer.test("1.2", "1.2,<EOF>", 116))
    def test_17(self):
        self.assertTrue(TestLexer.test("1.", "1.,<EOF>", 117))
    def test_18(self):
        self.assertTrue(TestLexer.test(".1", ".1,<EOF>", 118))
    def test_19(self):
        self.assertTrue(TestLexer.test("1e2", "1e2,<EOF>", 119))
    def test_20(self):
        self.assertTrue(TestLexer.test("128.E-42", "128.E-42,<EOF>", 120))

    def test_21(self):
        """test_whilestate_miss_body"""
        input= """procedure whilestate ();
        var x , y : real ; 
            begin
                if (a > 2) then printf(a) else printf(b) ;
            end"""
        expect='successful'
        self.assertTrue(TestLexer.test(input,expect,121))