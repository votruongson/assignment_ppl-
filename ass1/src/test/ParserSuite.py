import unittest
from TestUtils import TestParser

class ParserSuite(unittest.TestCase):
    # def test_var_decl3(self):
    #     """test var decl 3"""
    #     input = """var fa, da: integer;
    #                     _fads54, _41kd: real;
    #                     forbac , dasif: arraY [1 .. 3] of Integer;"""
    #     expect = "successful"
    #     self.assertTrue(TestParser.test(input,expect,201))

    # def test_var_decl4(self):
    #     """test var decl 4"""
    #     input = """var fa, da: integer;
    #                     _fads54, _41kd: real;
    #                 var mn_ , _da81: Boolean;
    #                     fsda : StrinG;
    #                     kk: INTEGER;"""
    #     expect = "successful"
    #     self.assertTrue(TestParser.test(input,expect,202))

    # def test_var_decl5(self):
    #     """test var decl with type error"""
    #     input = """var fa, da: interger;
    #                     _fads54, _41kd: real;"""
    #     expect = "Error on line 1 col 12: interger"
    #     self.assertTrue(TestParser.test(input,expect,203))

    # def test_var_decl_miss_type(self):
    #     """test var decl with type error"""
    #     input = """var fa, da;
    #                     _fads54, _41kd: real;"""
    #     expect = "Error on line 1 col 10: ;"
    #     self.assertTrue(TestParser.test(input,expect,204))

    # def test_var_decl_miss_colon(self):
    #     """test var decl with colon missing"""
    #     input = """var fa, da Integer;
    #                     _fads54, _41kd: real;"""
    #     expect = "Error on line 1 col 11: Integer"
    #     self.assertTrue(TestParser.test(input,expect,205))

    # def test_var_decl_miss(self):
    #     """test var decl with id list missing"""
    #     input = """var Integer;
    #                     _fads54, _41kd: real;"""
    #     expect = "Error on line 1 col 4: Integer"
    #     self.assertTrue(TestParser.test(input,expect,206))

    # """id error"""
    # def test_var_decl_id_error(self):
    #     """test var decl with id error"""
    #     input = """var 5342fd : Integer;
    #                     _fads54, _41kd: real;"""
    #     expect = "Error on line 1 col 4: 5342"
    #     self.assertTrue(TestParser.test(input,expect,207))

    # def test_normal_function_decl1(self):
    #     """test function decl normal 1"""
    #     input = """ function foo(a,b: integer ;c: real ): integer ;
    #                 var x,y: real ; 
    #                 begin
    #                 end """
    #     expect = "successful"
    #     self.assertTrue(TestParser.test(input, expect, 208))

    # def test_normal_function_decl2(self):
    #     """test function decl normal 2"""
    #     input = """ function _foo_432(a,b: integer ;c: real ): BooLean ;
    #                 var x,y: InTeger ; 
    #                 begin
    #                 end """
    #     expect = "successful"
    #     self.assertTrue(TestParser.test(input, expect, 209))

    # def test_normal_function_decl3(self):
    #     """test function decl normal 3"""
    #     input = """ function ffda5543oo(a, d_b: real ;c4143: Integer ): Real ;
    #                 var x,y: real ;
    #                 begin
    #                 end """
    #     expect = "successful"
    #     self.assertTrue(TestParser.test(input, expect, 210))

    # def test_normal_function_decl4(self):
    #     """test function decl normal 4"""
    #     input = """ function _fofdsao(a,b: integer ;c: real ): integer ;
    #                 var x,y: real ; 
    #                 begin
    #                 end """
    #     expect = "successful"
    #     self.assertTrue(TestParser.test(input, expect, 211))

    # def test_normal_function_decl5(self):
    #     """test function decl normal 5"""
    #     input = """ function foo(a,b: integer ;c: real ): integer ;
    #                 var x,y: real ; 
    #                 begin
    #                 end """
    #     expect = "successful"
    #     self.assertTrue(TestParser.test(input, expect, 212))

    # def test_normal_function_decl6(self):
    #     """test function decl normal 6"""
    #     input = """ function _2423foo(): integer ;
    #                 var x,y: real ; 
    #                 begin
    #                 end """
    #     expect = "successful"
    #     self.assertTrue(TestParser.test(input, expect, 213))

    # def test_array_type(self):
    #     """test array type """
    #     input = """ var da : array [1 .. 2] of integer;"""
    #     expect = "successful"
    #     self.assertTrue(TestParser.test(input, expect, 214))

    # def test_array_type2(self):
    #     """test array type 2"""
    #     input = """ var _da, HF74 : array [1 .. 2] of INTeger;"""
    #     expect = "successful"
    #     self.assertTrue(TestParser.test(input, expect, 215))

    # def test_normal_asignment_statament1(self):
    #     """test normal assignment statement 1"""
    #     input = """ function foo(a,b: integer ;c: real ): integer ;
    #                 var x,y: real ; 
    #                 begin
    #                     x := hda = 100 ;
    #                 end """
    #     expect = "successful"
    #     self.assertTrue(TestParser.test(input, expect, 216))
        
    # def test_procedure_declar(self):
    #     """test_procedure_declar"""
    #     input="""procedure abc ();
    #         var x , y : real ; 
    #         begin
            
    #         end"""
    #     expect = "successful"
    #     self.assertTrue(TestParser.test(input,expect,217))
    # def test_procedure_declar1(self):
    #     """test_procedure_declar"""
    #     input="""procedure abc ();
    #         var x , y : real ; 
    #         begin
            
    #         end"""
    #     expect = "successful"
    #     self.assertTrue(TestParser.test(input,expect,218))
    # def test_case_insensitive(self):
    #     """test_case_insensitive"""
    #     input="""prOceDure _ADVCDfoo ();
    #         var x , y : real ; 
    #         begin
            
    #         end"""
    #     expect = "successful"
    #     self.assertTrue(TestParser.test(input,expect,219))
    # def test_wrong_redundancy_colon(self):
    #     """test_wrong_redundancy_colon"""
    #     input="""procedure abc (): array [ 1 .. 2 ] of integer ;
    #         var x , y : real ; 
    #         begin
            
    #         end"""
    #     expect = "Error on line 1 col 16: :"
    #     self.assertTrue(TestParser.test(input,expect,220))
    # def test_wrong_miss_semi_produre(self):
    #     """test_wrong_redundancy_colon"""
    #     input="""procedure abc ()
    #         var x , y : real ; 
    #         begin
            
    #         end"""
    #     expect = "Error on line 2 col 12: var"
    #     self.assertTrue(TestParser.test(input,expect,221))
    # def test_wrong_miss_body_produre(self):
    #     """test_wrong_miss_body_produre """
    #     input="""procedure foo ();
    #         var x , y : real ;  """
           
    #     expect = "Error on line 2 col 32: <EOF>"
    #     self.assertTrue(TestParser.test(input,expect,222))
    # #test Assignment Statement
    # def test_assignment_stmt_simple(self):
    #     """test_assignment_stmt"""
    #     input= """procedure abc ();
    #         var x , y : real ; 
    #         begin
    #             a:=12;
    #         end"""
    #     expect='successful'
    #     self.assertTrue(TestParser.test(input,expect,223))
    # def test_assignment_stmt_many(self):
    #     """test_assignment_stmt_many"""
    #     input= """procedure abc ();
    #     var x , y : real ; 
    #         begin
    #             a:=b:=c:=d:=12;
    #         end"""
    #     expect='successful'
    #     self.assertTrue(TestParser.test(input,expect,224))
    # def test_assign_stmt_with_expr(self):
    #     """test_assign_stmt_with_expr"""
    #     input= """procedure abc ();
    #     var x , y : real ; 
    #         begin
    #             a:=b:=c:=d:=(12+3);
    #         end"""
    #     expect='successful'
    #     self.assertTrue(TestParser.test(input,expect,225))
    # def test_assign_stmt_with_funcall(self):
    #     """test_assign_stmt_with_expr"""
    #     input= """procedure abc ();
    #     var x , y : real ; 
    #         begin
    #             a:=b:=c:=d:=foo();
    #         end"""
    #     expect='successful'
    #     self.assertTrue(TestParser.test(input,expect,226))
    # def test_assign_stmt_with_funcall_semi(self):
    #     """test_assign_stmt_with_expr"""
    #     input= """procedure abc ();
    #     var x , y : real ; 
    #         begin
    #             a:=b:=c:=d:=foo(2+3;);
    #         end"""
    #     expect='Error on line 4 col 35: ;'
    #     self.assertTrue(TestParser.test(input,expect,227))
    # def test_assign_stmt_with_funcall_explist(self):
    #     """test_assign_stmt_with_funcall_explist"""
    #     input= """procedure abc ();
    #     var x , y : real ; 
    #         begin
    #             a:=b:=c:=d:=foo(2+3, 3*3, a AND b);
    #         end"""
    #     expect='successful'
    #     self.assertTrue(TestParser.test(input,expect,228))
    # def test_assign_stmt_with_realtype(self):
    #     """test_assign_stmt_with_funcall_explist"""
    #     input= """procedure abc ();
    #     var x , y : real ; 
    #         begin
    #             some_real := 37573.5 * 37593 + 385.8 / 367.1;
    #         end"""
    #     expect='successful'
    #     self.assertTrue(TestParser.test(input,expect,229))
        
    # #test_if_state
    # def test_ifstate(self):
    #     """test_ifstate"""
    #     input= """procedure abc ();
    #     var x , y : real ; 
    #         begin
    #             if color = red then
    #                a:=3;
    #             else
    #                 b:=3;
    #         end"""
    #     expect='successful'
    #     self.assertTrue(TestParser.test(input,expect,230))
    # def test_ifstate_miss_else(self):
    #     """test_ifstate_miss_else"""
    #     input= """procedure abc ();
    #     var x , y : real ; 
    #         begin
    #             if color = red then
    #                 b:=3;
    #         end"""
    #     expect='successful'
    #     self.assertTrue(TestParser.test(input,expect,231))
    # def test_ifstate_miss_bodyif(self):
    #     """test_ifstate_miss_bodyif"""
    #     input= """procedure abc ();
    #     var x , y : real ; 
    #         begin
    #             if color = red then
    #         end"""
    #     expect='Error on line 5 col 12: end'
    #     self.assertTrue(TestParser.test(input,expect,232))
    # def test_ifstate_miss_exp(self):
    #     """test_ifstate_miss_exp"""
    #     input= """procedure abc ();
    #     var x , y : real ; 
    #         begin
    #             if  then
    #                 b:=3;
    #         end"""
    #     expect='Error on line 4 col 20: then'
    #     self.assertTrue(TestParser.test(input,expect,233))
    # def test_ifstate_wrong_exp(self):
    #     """test_ifstate_wrong_exp"""
    #     input= """procedure abc ();
    #     var x , y : real ; 
    #         begin
    #             if +hello then
    #                 b:=3;
    #         end"""
    #     expect='Error on line 4 col 19: +'
    #     self.assertTrue(TestParser.test(input,expect,234))
    # def test_ifstate_wrong_exp_real(self):
    #     """test_ifstate_wrong_exp_real"""
    #     input= """procedure abc ();
    #     var x , y : real ; 
    #         begin
    #             if 3//4 then
    #                 b:=3;
    #         end"""
    #     expect='Error on line 5 col 20: b'
    #     self.assertTrue(TestParser.test(input,expect,235))
    # def test_ifstate_have_semi(self):
    #     """test_ifstate_have_semi"""
    #     input= """procedure abc ();
    #     var x , y : real ; 
    #         begin
    #             if color = red then;
    #                 hk:=56;
    #         end"""
    #     expect='Error on line 4 col 35: ;'
    #     self.assertTrue(TestParser.test(input,expect,236))
    # def test_ifstate_have_semi_else(self):
    #     """test_ifstate_have_semi_else"""
    #     input= """procedure abc ();
    #     var x , y : real ; 
    #         begin
    #             if color = red then
    #                a:=3;
    #             else;
    #                 b:=3;
    #         end"""
    #     expect='Error on line 6 col 16: else'
    #     self.assertTrue(TestParser.test(input,expect,237))
    # #test_for_state 
    # def test_for_state_to(self):
    #     """test_for_state_to"""
    #     input= """procedure ABC ();
    #     var x , y : real ; 
    #         begin
    #             for i:= 1 to 10 do g:=5;
    #         end"""
    #     expect='successful'
    #     self.assertTrue(TestParser.test(input,expect,238))
    # def test_for_state_downto(self):
    #     """test_for_state_downto"""
    #     input= """procedure ABC ();
    #     var x , y : real ; 
    #         begin
    #             for i:= 1 downto 10 do g:=5;
    #         end"""
    #     expect='successful'
    #     self.assertTrue(TestParser.test(input,expect,239))
    # def test_for_state_miss_colon(self):
    #     """test_for_state_miss_colon"""
    #     input= """procedure ABC ();
    #     var x , y : real ; 
    #         begin
    #             for i= 1 downto 10 do g:=5;
    #         end"""
    #     expect='Error on line 4 col 21: ='
    #     self.assertTrue(TestParser.test(input,expect,240))

    # def test_full_program(self):
    #     """test full program"""
    #     input= """var i : integer ;
    #                 function f () : integer ;
    #                     begin
    #                         return 200;
    #                     end
    #                 procedure main () ;
    #                 var
    #                      main : integer ;
    #                 begin
    #                     main := f () ;
    #                     putIntLn ( main ) ;
    #                      with
    #                         i : integer ;
    #                         main : integer ;
    #                         f : integer ;
    #                     do begin
    #                         main := f := i := 100 ;
    #                         putIntLn ( i ) ;
    #                         putIntLn ( main ) ;
    #                         putIntLn ( f ) ;
    #                     end
    #                     putIntLn ( main ) ;
    #                 end
    #                 var g : real ;"""
    #     expect='successful'
    #     self.assertTrue(TestParser.test(input,expect,241))

    # def test_for_state_wrong_downto(self):
    #     """test_for_state_wrong_downto"""
    #     input= """procedure ABC ();
    #     var x , y : real ; 
    #         begin
    #             for i:= 1 dwnto 10 do g:=5;
    #         end"""
    #     expect='Error on line 4 col 26: dwnto'
    #     self.assertTrue(TestParser.test(input,expect,242))
    # def test_for_state_miss_assign(self):
    #     """test_for_state_miss_assign"""
    #     input= """procedure ABC ();
    #     var x , y : real ; 
    #         begin
    #             for dwnto 10 do g:=5;
    #         end"""
    #     expect='Error on line 4 col 26: 10'
    #     self.assertTrue(TestParser.test(input,expect,243))
    # def test_for_state_miss_stmt(self):
    #     """test_for_state_miss_stmt"""
    #     input= """procedure ABC ();
    #     var x , y : real ; 
    #         begin
    #             for i:= 1 downto 10 do ;
    #         end"""
    #     expect='Error on line 4 col 39: ;'
    #     self.assertTrue(TestParser.test(input,expect,244))
    # def test_for_state_many_stmt(self):
    #     """test_for_state_many_stmt"""
    #     input= """procedure ABC ();
    #     var x , y : real ; 
    #         begin
    #             for i:= 1 downto 10 do a := 3; d := 5;
    #             if color = red then
    #                a:=3;
    #         end"""
    #     expect='successful'
    #     self.assertTrue(TestParser.test(input,expect,245))
    # def test_for_state_named(self):
    #     """test_for_state_named"""
    #     input= """procedure ABC ();
    #     var x , y : real ; 
    #         begin
    #             for 123:= 1 downto 10 do a := 3; d := 5;
    #         end"""
    #     expect='Error on line 4 col 20: 123'
    #     self.assertTrue(TestParser.test(input,expect,246))
    # #test while state
    # def test_while_state(self):
    #     """test_while_tate"""
    #     input= """procedure whilestate ();
    #     var x , y : real ; 
    #         begin
    #             while a = true do b := 5; d := 24; d := 25; ;
    #         end"""
    #     expect='Error on line 4 col 60: ;'
    #     self.assertTrue(TestParser.test(input,expect,247))
    # def test_whilestate_many_stmt(self):
    #     """test_whilestate_many_stmt"""
    #     input= """procedure whilestate ();
    #     var x , y : real ; 
    #         begin
    #             while a = true do b := 5; d := 24; d := 25;
    #         end"""
    #     expect='successful'
    #     self.assertTrue(TestParser.test(input,expect,248))
    # def test_whilestate_wrong_expr(self):
    #     """test_whilestate_wrong_expr"""
    #     input= """procedure whilestate ();
    #     var x , y : real ; 
    #         begin
    #             while a := true do b := 5;;
    #         end"""
    #     expect='Error on line 4 col 24: :='
    #     self.assertTrue(TestParser.test(input,expect,249))
    # def test_whilestate_miss_body(self):
    #     """test_whilestate_miss_body"""
    #     input= """procedure whilestate ();
    #     var x , y : real ; 
    #         begin
    #             while a = true do ;
    #         end"""
    #     expect='Error on line 4 col 34: ;'
    #     self.assertTrue(TestParser.test(input,expect,250))
    # def test_andthen(self):
    #     """test_whilestate_miss_body"""
    #     input= """procedure whilestate ();
    #     var x , y : real ; 
    #         begin
    #             a := b and then 2 ;
    #         end"""
    #     expect='successful'
    #     self.assertTrue(TestParser.test(input,expect,251))
    # def test_orelse(self):
    #     """test_whilestate_miss_body"""
    #     input= """procedure whilestate ();
    #     var x , y : real ; 
    #         begin
    #             a := b or else  2 ;
    #         end"""
    #     expect='successful'
    #     self.assertTrue(TestParser.test(input,expect,252))
    def test_ifelse(self):
        """test_whilestate_miss_body"""
        input= """procedure whilestate ();
        var x , y : real ; 
            begin
                if (a > 2) then 1 else 1;
            end"""
        expect='successful'
        self.assertTrue(TestParser.test(input,expect,253))

   





