# Generated from main/mp/parser/MP.g4 by ANTLR 4.7.1
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .MPParser import MPParser
else:
    from MPParser import MPParser

# This class defines a complete generic visitor for a parse tree produced by MPParser.

class MPVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by MPParser#program.
    def visitProgram(self, ctx:MPParser.ProgramContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#declaration.
    def visitDeclaration(self, ctx:MPParser.DeclarationContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#variable.
    def visitVariable(self, ctx:MPParser.VariableContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#function.
    def visitFunction(self, ctx:MPParser.FunctionContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#procedure.
    def visitProcedure(self, ctx:MPParser.ProcedureContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#para_list.
    def visitPara_list(self, ctx:MPParser.Para_listContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#para_decl.
    def visitPara_decl(self, ctx:MPParser.Para_declContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#mptype.
    def visitMptype(self, ctx:MPParser.MptypeContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#pritype.
    def visitPritype(self, ctx:MPParser.PritypeContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#comptype.
    def visitComptype(self, ctx:MPParser.ComptypeContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#compound_stmt.
    def visitCompound_stmt(self, ctx:MPParser.Compound_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#stmt_part.
    def visitStmt_part(self, ctx:MPParser.Stmt_partContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#stmt.
    def visitStmt(self, ctx:MPParser.StmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#match_if.
    def visitMatch_if(self, ctx:MPParser.Match_ifContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#unmatch_if.
    def visitUnmatch_if(self, ctx:MPParser.Unmatch_ifContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#dowhile_stmt.
    def visitDowhile_stmt(self, ctx:MPParser.Dowhile_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#for_stmt.
    def visitFor_stmt(self, ctx:MPParser.For_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#with_stmt.
    def visitWith_stmt(self, ctx:MPParser.With_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#other.
    def visitOther(self, ctx:MPParser.OtherContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#assign_stmt.
    def visitAssign_stmt(self, ctx:MPParser.Assign_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#break_stmt.
    def visitBreak_stmt(self, ctx:MPParser.Break_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#continue_stmt.
    def visitContinue_stmt(self, ctx:MPParser.Continue_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#return_stmt.
    def visitReturn_stmt(self, ctx:MPParser.Return_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#para_with_list.
    def visitPara_with_list(self, ctx:MPParser.Para_with_listContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#call_stmt.
    def visitCall_stmt(self, ctx:MPParser.Call_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#exp.
    def visitExp(self, ctx:MPParser.ExpContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#exp1.
    def visitExp1(self, ctx:MPParser.Exp1Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#exp2.
    def visitExp2(self, ctx:MPParser.Exp2Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#exp3.
    def visitExp3(self, ctx:MPParser.Exp3Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#exp4.
    def visitExp4(self, ctx:MPParser.Exp4Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#exp5.
    def visitExp5(self, ctx:MPParser.Exp5Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#exp6.
    def visitExp6(self, ctx:MPParser.Exp6Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#func_call.
    def visitFunc_call(self, ctx:MPParser.Func_callContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#listexp.
    def visitListexp(self, ctx:MPParser.ListexpContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#literal.
    def visitLiteral(self, ctx:MPParser.LiteralContext):
        return self.visitChildren(ctx)



del MPParser