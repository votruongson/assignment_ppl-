# Generated from main/mp/parser/MP.g4 by ANTLR 4.7.1
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


from lexererr import *


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2?")
        buf.write("\u01f1\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23")
        buf.write("\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30")
        buf.write("\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36")
        buf.write("\t\36\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%")
        buf.write("\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4,\t,\4-\t-\4.")
        buf.write("\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64")
        buf.write("\t\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:")
        buf.write("\4;\t;\4<\t<\4=\t=\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\t")
        buf.write("C\3\2\6\2\u0089\n\2\r\2\16\2\u008a\3\2\3\2\3\3\6\3\u0090")
        buf.write("\n\3\r\3\16\3\u0091\3\4\3\4\3\4\3\5\6\5\u0098\n\5\r\5")
        buf.write("\16\5\u0099\3\5\5\5\u009d\n\5\3\5\7\5\u00a0\n\5\f\5\16")
        buf.write("\5\u00a3\13\5\3\5\7\5\u00a6\n\5\f\5\16\5\u00a9\13\5\3")
        buf.write("\5\3\5\6\5\u00ad\n\5\r\5\16\5\u00ae\5\5\u00b1\n\5\3\5")
        buf.write("\3\5\5\5\u00b5\n\5\3\5\6\5\u00b8\n\5\r\5\16\5\u00b9\5")
        buf.write("\5\u00bc\n\5\3\6\3\6\5\6\u00c0\n\6\3\7\3\7\3\7\3\7\3\7")
        buf.write("\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3")
        buf.write("\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13")
        buf.write("\3\13\3\13\3\13\3\f\3\f\3\f\3\r\3\r\3\r\3\16\3\16\3\16")
        buf.write("\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3\20\3\20\3\20\3\20")
        buf.write("\3\21\3\21\3\21\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\23")
        buf.write("\3\23\3\23\3\24\3\24\3\24\3\24\3\24\3\24\3\25\3\25\3\25")
        buf.write("\3\25\3\25\3\25\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26")
        buf.write("\3\26\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27")
        buf.write("\3\30\3\30\3\30\3\30\3\31\3\31\3\31\3\31\3\32\3\32\3\32")
        buf.write("\3\32\3\32\3\33\3\33\3\33\3\33\3\33\3\34\3\34\3\34\3\34")
        buf.write("\3\34\3\34\3\35\3\35\3\35\3\35\3\35\3\35\3\36\3\36\3\36")
        buf.write("\3\36\3\36\3\36\3\36\3\36\3\36\3\37\3\37\3\37\3\37\3\37")
        buf.write("\3\37\3\37\3 \3 \3 \3 \3!\3!\3!\3!\3\"\3\"\3\"\3\"\3#")
        buf.write("\3#\3#\3#\3$\3$\3$\3%\3%\7%\u0167\n%\f%\16%\u016a\13%")
        buf.write("\3&\3&\3&\3\'\3\'\3\'\3(\3(\3(\7(\u0175\n(\f(\16(\u0178")
        buf.write("\13(\3(\3(\3)\3)\3)\7)\u017f\n)\f)\16)\u0182\13)\3)\3")
        buf.write(")\3)\7)\u0187\n)\f)\16)\u018a\13)\6)\u018c\n)\r)\16)\u018d")
        buf.write("\3)\3)\3)\3*\3*\3*\7*\u0196\n*\f*\16*\u0199\13*\3*\3*")
        buf.write("\3*\3+\3+\3+\5+\u01a1\n+\3+\3+\3,\3,\3,\3,\7,\u01a9\n")
        buf.write(",\f,\16,\u01ac\13,\3,\3,\3,\3-\3-\7-\u01b3\n-\f-\16-\u01b6")
        buf.write("\13-\3-\3-\3.\3.\3.\3.\7.\u01be\n.\f.\16.\u01c1\13.\3")
        buf.write("/\3/\3\60\3\60\3\61\3\61\3\62\3\62\3\63\3\63\3\64\3\64")
        buf.write("\3\65\3\65\3\66\3\66\3\67\3\67\38\38\39\39\3:\3:\3;\3")
        buf.write(";\3<\3<\3<\3=\3=\3>\3>\3>\3?\3?\3@\3@\3@\3A\3A\3B\3B\3")
        buf.write("B\3C\3C\3C\4\u01aa\u01b4\2D\3\3\5\4\7\5\t\6\13\7\r\b\17")
        buf.write("\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20\37\21!\22#\23")
        buf.write("%\24\'\25)\26+\27-\30/\31\61\32\63\33\65\34\67\359\36")
        buf.write(";\37= ?!A\"C#E$G%I&K\2M\2O\'Q(S)U*W\2Y\2[\2]+_,a-c.e/")
        buf.write("g\60i\61k\62m\63o\64q\65s\66u\67w8y9{:};\177<\u0081=\u0083")
        buf.write(">\u0085?\3\2 \5\2\13\f\17\17\"\"\3\2\62;\4\2GGgg\4\2K")
        buf.write("Kkk\4\2PPpp\4\2VVvv\4\2IIii\4\2TTtt\4\2CCcc\4\2NNnn\4")
        buf.write("\2DDdd\4\2QQqq\4\2UUuu\4\2[[{{\4\2HHhh\4\2JJjj\4\2FFf")
        buf.write("f\4\2YYyy\4\2WWww\4\2EEee\4\2RRrr\4\2XXxx\4\2MMmm\4\2")
        buf.write("OOoo\5\2C\\aac|\6\2\62;C\\aac|\n\2$$))^^ddhhppttvv\5\2")
        buf.write("\f\f$$^^\6\2\f\f\17\17$$^^\4\2\f\f\17\17\2\u0206\2\3\3")
        buf.write("\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2")
        buf.write("\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2")
        buf.write("\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2")
        buf.write("\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2")
        buf.write("\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3")
        buf.write("\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2")
        buf.write("\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3")
        buf.write("\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2O")
        buf.write("\3\2\2\2\2Q\3\2\2\2\2S\3\2\2\2\2U\3\2\2\2\2]\3\2\2\2\2")
        buf.write("_\3\2\2\2\2a\3\2\2\2\2c\3\2\2\2\2e\3\2\2\2\2g\3\2\2\2")
        buf.write("\2i\3\2\2\2\2k\3\2\2\2\2m\3\2\2\2\2o\3\2\2\2\2q\3\2\2")
        buf.write("\2\2s\3\2\2\2\2u\3\2\2\2\2w\3\2\2\2\2y\3\2\2\2\2{\3\2")
        buf.write("\2\2\2}\3\2\2\2\2\177\3\2\2\2\2\u0081\3\2\2\2\2\u0083")
        buf.write("\3\2\2\2\2\u0085\3\2\2\2\3\u0088\3\2\2\2\5\u008f\3\2\2")
        buf.write("\2\7\u0093\3\2\2\2\t\u00b0\3\2\2\2\13\u00bf\3\2\2\2\r")
        buf.write("\u00c1\3\2\2\2\17\u00c9\3\2\2\2\21\u00ce\3\2\2\2\23\u00d6")
        buf.write("\3\2\2\2\25\u00dd\3\2\2\2\27\u00e3\3\2\2\2\31\u00e6\3")
        buf.write("\2\2\2\33\u00e9\3\2\2\2\35\u00ee\3\2\2\2\37\u00f3\3\2")
        buf.write("\2\2!\u00f7\3\2\2\2#\u00fa\3\2\2\2%\u0101\3\2\2\2\'\u0104")
        buf.write("\3\2\2\2)\u010a\3\2\2\2+\u0110\3\2\2\2-\u0119\3\2\2\2")
        buf.write("/\u0123\3\2\2\2\61\u0127\3\2\2\2\63\u012b\3\2\2\2\65\u0130")
        buf.write("\3\2\2\2\67\u0135\3\2\2\29\u013b\3\2\2\2;\u0141\3\2\2")
        buf.write("\2=\u014a\3\2\2\2?\u0151\3\2\2\2A\u0155\3\2\2\2C\u0159")
        buf.write("\3\2\2\2E\u015d\3\2\2\2G\u0161\3\2\2\2I\u0164\3\2\2\2")
        buf.write("K\u016b\3\2\2\2M\u016e\3\2\2\2O\u0171\3\2\2\2Q\u017b\3")
        buf.write("\2\2\2S\u0192\3\2\2\2U\u01a0\3\2\2\2W\u01a4\3\2\2\2Y\u01b0")
        buf.write("\3\2\2\2[\u01b9\3\2\2\2]\u01c2\3\2\2\2_\u01c4\3\2\2\2")
        buf.write("a\u01c6\3\2\2\2c\u01c8\3\2\2\2e\u01ca\3\2\2\2g\u01cc\3")
        buf.write("\2\2\2i\u01ce\3\2\2\2k\u01d0\3\2\2\2m\u01d2\3\2\2\2o\u01d4")
        buf.write("\3\2\2\2q\u01d6\3\2\2\2s\u01d8\3\2\2\2u\u01da\3\2\2\2")
        buf.write("w\u01dc\3\2\2\2y\u01df\3\2\2\2{\u01e1\3\2\2\2}\u01e4\3")
        buf.write("\2\2\2\177\u01e6\3\2\2\2\u0081\u01e9\3\2\2\2\u0083\u01eb")
        buf.write("\3\2\2\2\u0085\u01ee\3\2\2\2\u0087\u0089\t\2\2\2\u0088")
        buf.write("\u0087\3\2\2\2\u0089\u008a\3\2\2\2\u008a\u0088\3\2\2\2")
        buf.write("\u008a\u008b\3\2\2\2\u008b\u008c\3\2\2\2\u008c\u008d\b")
        buf.write("\2\2\2\u008d\4\3\2\2\2\u008e\u0090\t\3\2\2\u008f\u008e")
        buf.write("\3\2\2\2\u0090\u0091\3\2\2\2\u0091\u008f\3\2\2\2\u0091")
        buf.write("\u0092\3\2\2\2\u0092\6\3\2\2\2\u0093\u0094\7\60\2\2\u0094")
        buf.write("\u0095\7\60\2\2\u0095\b\3\2\2\2\u0096\u0098\t\3\2\2\u0097")
        buf.write("\u0096\3\2\2\2\u0098\u0099\3\2\2\2\u0099\u0097\3\2\2\2")
        buf.write("\u0099\u009a\3\2\2\2\u009a\u009c\3\2\2\2\u009b\u009d\7")
        buf.write("\60\2\2\u009c\u009b\3\2\2\2\u009c\u009d\3\2\2\2\u009d")
        buf.write("\u00a1\3\2\2\2\u009e\u00a0\t\3\2\2\u009f\u009e\3\2\2\2")
        buf.write("\u00a0\u00a3\3\2\2\2\u00a1\u009f\3\2\2\2\u00a1\u00a2\3")
        buf.write("\2\2\2\u00a2\u00b1\3\2\2\2\u00a3\u00a1\3\2\2\2\u00a4\u00a6")
        buf.write("\t\3\2\2\u00a5\u00a4\3\2\2\2\u00a6\u00a9\3\2\2\2\u00a7")
        buf.write("\u00a5\3\2\2\2\u00a7\u00a8\3\2\2\2\u00a8\u00aa\3\2\2\2")
        buf.write("\u00a9\u00a7\3\2\2\2\u00aa\u00ac\7\60\2\2\u00ab\u00ad")
        buf.write("\t\3\2\2\u00ac\u00ab\3\2\2\2\u00ad\u00ae\3\2\2\2\u00ae")
        buf.write("\u00ac\3\2\2\2\u00ae\u00af\3\2\2\2\u00af\u00b1\3\2\2\2")
        buf.write("\u00b0\u0097\3\2\2\2\u00b0\u00a7\3\2\2\2\u00b1\u00bb\3")
        buf.write("\2\2\2\u00b2\u00b4\t\4\2\2\u00b3\u00b5\7/\2\2\u00b4\u00b3")
        buf.write("\3\2\2\2\u00b4\u00b5\3\2\2\2\u00b5\u00b7\3\2\2\2\u00b6")
        buf.write("\u00b8\t\3\2\2\u00b7\u00b6\3\2\2\2\u00b8\u00b9\3\2\2\2")
        buf.write("\u00b9\u00b7\3\2\2\2\u00b9\u00ba\3\2\2\2\u00ba\u00bc\3")
        buf.write("\2\2\2\u00bb\u00b2\3\2\2\2\u00bb\u00bc\3\2\2\2\u00bc\n")
        buf.write("\3\2\2\2\u00bd\u00c0\5\65\33\2\u00be\u00c0\5\67\34\2\u00bf")
        buf.write("\u00bd\3\2\2\2\u00bf\u00be\3\2\2\2\u00c0\f\3\2\2\2\u00c1")
        buf.write("\u00c2\t\5\2\2\u00c2\u00c3\t\6\2\2\u00c3\u00c4\t\7\2\2")
        buf.write("\u00c4\u00c5\t\4\2\2\u00c5\u00c6\t\b\2\2\u00c6\u00c7\t")
        buf.write("\4\2\2\u00c7\u00c8\t\t\2\2\u00c8\16\3\2\2\2\u00c9\u00ca")
        buf.write("\t\t\2\2\u00ca\u00cb\t\4\2\2\u00cb\u00cc\t\n\2\2\u00cc")
        buf.write("\u00cd\t\13\2\2\u00cd\20\3\2\2\2\u00ce\u00cf\t\f\2\2\u00cf")
        buf.write("\u00d0\t\r\2\2\u00d0\u00d1\t\r\2\2\u00d1\u00d2\t\13\2")
        buf.write("\2\u00d2\u00d3\t\4\2\2\u00d3\u00d4\t\n\2\2\u00d4\u00d5")
        buf.write("\t\6\2\2\u00d5\22\3\2\2\2\u00d6\u00d7\t\16\2\2\u00d7\u00d8")
        buf.write("\t\7\2\2\u00d8\u00d9\t\t\2\2\u00d9\u00da\t\5\2\2\u00da")
        buf.write("\u00db\t\6\2\2\u00db\u00dc\t\b\2\2\u00dc\24\3\2\2\2\u00dd")
        buf.write("\u00de\t\n\2\2\u00de\u00df\t\t\2\2\u00df\u00e0\t\t\2\2")
        buf.write("\u00e0\u00e1\t\n\2\2\u00e1\u00e2\t\17\2\2\u00e2\26\3\2")
        buf.write("\2\2\u00e3\u00e4\t\r\2\2\u00e4\u00e5\t\20\2\2\u00e5\30")
        buf.write("\3\2\2\2\u00e6\u00e7\t\5\2\2\u00e7\u00e8\t\20\2\2\u00e8")
        buf.write("\32\3\2\2\2\u00e9\u00ea\t\4\2\2\u00ea\u00eb\t\13\2\2\u00eb")
        buf.write("\u00ec\t\16\2\2\u00ec\u00ed\t\4\2\2\u00ed\34\3\2\2\2\u00ee")
        buf.write("\u00ef\t\7\2\2\u00ef\u00f0\t\21\2\2\u00f0\u00f1\t\4\2")
        buf.write("\2\u00f1\u00f2\t\6\2\2\u00f2\36\3\2\2\2\u00f3\u00f4\t")
        buf.write("\20\2\2\u00f4\u00f5\t\r\2\2\u00f5\u00f6\t\t\2\2\u00f6")
        buf.write(" \3\2\2\2\u00f7\u00f8\t\7\2\2\u00f8\u00f9\t\r\2\2\u00f9")
        buf.write("\"\3\2\2\2\u00fa\u00fb\t\22\2\2\u00fb\u00fc\t\r\2\2\u00fc")
        buf.write("\u00fd\t\23\2\2\u00fd\u00fe\t\6\2\2\u00fe\u00ff\t\7\2")
        buf.write("\2\u00ff\u0100\t\r\2\2\u0100$\3\2\2\2\u0101\u0102\t\22")
        buf.write("\2\2\u0102\u0103\t\r\2\2\u0103&\3\2\2\2\u0104\u0105\t")
        buf.write("\23\2\2\u0105\u0106\t\21\2\2\u0106\u0107\t\5\2\2\u0107")
        buf.write("\u0108\t\13\2\2\u0108\u0109\t\4\2\2\u0109(\3\2\2\2\u010a")
        buf.write("\u010b\t\f\2\2\u010b\u010c\t\4\2\2\u010c\u010d\t\b\2\2")
        buf.write("\u010d\u010e\t\5\2\2\u010e\u010f\t\6\2\2\u010f*\3\2\2")
        buf.write("\2\u0110\u0111\t\20\2\2\u0111\u0112\t\24\2\2\u0112\u0113")
        buf.write("\t\6\2\2\u0113\u0114\t\25\2\2\u0114\u0115\t\7\2\2\u0115")
        buf.write("\u0116\t\5\2\2\u0116\u0117\t\r\2\2\u0117\u0118\t\6\2\2")
        buf.write("\u0118,\3\2\2\2\u0119\u011a\t\26\2\2\u011a\u011b\t\t\2")
        buf.write("\2\u011b\u011c\t\r\2\2\u011c\u011d\t\25\2\2\u011d\u011e")
        buf.write("\t\4\2\2\u011e\u011f\t\22\2\2\u011f\u0120\t\24\2\2\u0120")
        buf.write("\u0121\t\t\2\2\u0121\u0122\t\4\2\2\u0122.\3\2\2\2\u0123")
        buf.write("\u0124\t\27\2\2\u0124\u0125\t\n\2\2\u0125\u0126\t\t\2")
        buf.write("\2\u0126\60\3\2\2\2\u0127\u0128\t\4\2\2\u0128\u0129\t")
        buf.write("\6\2\2\u0129\u012a\t\22\2\2\u012a\62\3\2\2\2\u012b\u012c")
        buf.write("\t\23\2\2\u012c\u012d\t\5\2\2\u012d\u012e\t\7\2\2\u012e")
        buf.write("\u012f\t\21\2\2\u012f\64\3\2\2\2\u0130\u0131\t\7\2\2\u0131")
        buf.write("\u0132\t\t\2\2\u0132\u0133\t\24\2\2\u0133\u0134\t\4\2")
        buf.write("\2\u0134\66\3\2\2\2\u0135\u0136\t\20\2\2\u0136\u0137\t")
        buf.write("\n\2\2\u0137\u0138\t\13\2\2\u0138\u0139\t\16\2\2\u0139")
        buf.write("\u013a\t\4\2\2\u013a8\3\2\2\2\u013b\u013c\t\f\2\2\u013c")
        buf.write("\u013d\t\t\2\2\u013d\u013e\t\4\2\2\u013e\u013f\t\n\2\2")
        buf.write("\u013f\u0140\t\30\2\2\u0140:\3\2\2\2\u0141\u0142\t\25")
        buf.write("\2\2\u0142\u0143\t\r\2\2\u0143\u0144\t\6\2\2\u0144\u0145")
        buf.write("\t\7\2\2\u0145\u0146\t\5\2\2\u0146\u0147\t\6\2\2\u0147")
        buf.write("\u0148\t\24\2\2\u0148\u0149\t\4\2\2\u0149<\3\2\2\2\u014a")
        buf.write("\u014b\t\t\2\2\u014b\u014c\t\4\2\2\u014c\u014d\t\7\2\2")
        buf.write("\u014d\u014e\t\24\2\2\u014e\u014f\t\t\2\2\u014f\u0150")
        buf.write("\t\6\2\2\u0150>\3\2\2\2\u0151\u0152\t\31\2\2\u0152\u0153")
        buf.write("\t\r\2\2\u0153\u0154\t\22\2\2\u0154@\3\2\2\2\u0155\u0156")
        buf.write("\t\22\2\2\u0156\u0157\t\5\2\2\u0157\u0158\t\27\2\2\u0158")
        buf.write("B\3\2\2\2\u0159\u015a\t\6\2\2\u015a\u015b\t\r\2\2\u015b")
        buf.write("\u015c\t\7\2\2\u015cD\3\2\2\2\u015d\u015e\t\n\2\2\u015e")
        buf.write("\u015f\t\6\2\2\u015f\u0160\t\22\2\2\u0160F\3\2\2\2\u0161")
        buf.write("\u0162\t\r\2\2\u0162\u0163\t\t\2\2\u0163H\3\2\2\2\u0164")
        buf.write("\u0168\t\32\2\2\u0165\u0167\t\33\2\2\u0166\u0165\3\2\2")
        buf.write("\2\u0167\u016a\3\2\2\2\u0168\u0166\3\2\2\2\u0168\u0169")
        buf.write("\3\2\2\2\u0169J\3\2\2\2\u016a\u0168\3\2\2\2\u016b\u016c")
        buf.write("\7^\2\2\u016c\u016d\t\34\2\2\u016dL\3\2\2\2\u016e\u016f")
        buf.write("\7^\2\2\u016f\u0170\n\34\2\2\u0170N\3\2\2\2\u0171\u0176")
        buf.write("\7$\2\2\u0172\u0175\5K&\2\u0173\u0175\n\35\2\2\u0174\u0172")
        buf.write("\3\2\2\2\u0174\u0173\3\2\2\2\u0175\u0178\3\2\2\2\u0176")
        buf.write("\u0174\3\2\2\2\u0176\u0177\3\2\2\2\u0177\u0179\3\2\2\2")
        buf.write("\u0178\u0176\3\2\2\2\u0179\u017a\b(\3\2\u017aP\3\2\2\2")
        buf.write("\u017b\u018b\7$\2\2\u017c\u017f\5K&\2\u017d\u017f\n\35")
        buf.write("\2\2\u017e\u017c\3\2\2\2\u017e\u017d\3\2\2\2\u017f\u0182")
        buf.write("\3\2\2\2\u0180\u017e\3\2\2\2\u0180\u0181\3\2\2\2\u0181")
        buf.write("\u0183\3\2\2\2\u0182\u0180\3\2\2\2\u0183\u0188\5M\'\2")
        buf.write("\u0184\u0187\5K&\2\u0185\u0187\n\36\2\2\u0186\u0184\3")
        buf.write("\2\2\2\u0186\u0185\3\2\2\2\u0187\u018a\3\2\2\2\u0188\u0186")
        buf.write("\3\2\2\2\u0188\u0189\3\2\2\2\u0189\u018c\3\2\2\2\u018a")
        buf.write("\u0188\3\2\2\2\u018b\u0180\3\2\2\2\u018c\u018d\3\2\2\2")
        buf.write("\u018d\u018b\3\2\2\2\u018d\u018e\3\2\2\2\u018e\u018f\3")
        buf.write("\2\2\2\u018f\u0190\7$\2\2\u0190\u0191\b)\4\2\u0191R\3")
        buf.write("\2\2\2\u0192\u0197\7$\2\2\u0193\u0196\5K&\2\u0194\u0196")
        buf.write("\n\35\2\2\u0195\u0193\3\2\2\2\u0195\u0194\3\2\2\2\u0196")
        buf.write("\u0199\3\2\2\2\u0197\u0195\3\2\2\2\u0197\u0198\3\2\2\2")
        buf.write("\u0198\u019a\3\2\2\2\u0199\u0197\3\2\2\2\u019a\u019b\7")
        buf.write("$\2\2\u019b\u019c\b*\5\2\u019cT\3\2\2\2\u019d\u01a1\5")
        buf.write("W,\2\u019e\u01a1\5Y-\2\u019f\u01a1\5[.\2\u01a0\u019d\3")
        buf.write("\2\2\2\u01a0\u019e\3\2\2\2\u01a0\u019f\3\2\2\2\u01a1\u01a2")
        buf.write("\3\2\2\2\u01a2\u01a3\b+\2\2\u01a3V\3\2\2\2\u01a4\u01a5")
        buf.write("\7*\2\2\u01a5\u01a6\7,\2\2\u01a6\u01aa\3\2\2\2\u01a7\u01a9")
        buf.write("\13\2\2\2\u01a8\u01a7\3\2\2\2\u01a9\u01ac\3\2\2\2\u01aa")
        buf.write("\u01ab\3\2\2\2\u01aa\u01a8\3\2\2\2\u01ab\u01ad\3\2\2\2")
        buf.write("\u01ac\u01aa\3\2\2\2\u01ad\u01ae\7,\2\2\u01ae\u01af\7")
        buf.write("+\2\2\u01afX\3\2\2\2\u01b0\u01b4\7}\2\2\u01b1\u01b3\13")
        buf.write("\2\2\2\u01b2\u01b1\3\2\2\2\u01b3\u01b6\3\2\2\2\u01b4\u01b5")
        buf.write("\3\2\2\2\u01b4\u01b2\3\2\2\2\u01b5\u01b7\3\2\2\2\u01b6")
        buf.write("\u01b4\3\2\2\2\u01b7\u01b8\7\177\2\2\u01b8Z\3\2\2\2\u01b9")
        buf.write("\u01ba\7\61\2\2\u01ba\u01bb\7\61\2\2\u01bb\u01bf\3\2\2")
        buf.write("\2\u01bc\u01be\n\37\2\2\u01bd\u01bc\3\2\2\2\u01be\u01c1")
        buf.write("\3\2\2\2\u01bf\u01bd\3\2\2\2\u01bf\u01c0\3\2\2\2\u01c0")
        buf.write("\\\3\2\2\2\u01c1\u01bf\3\2\2\2\u01c2\u01c3\7*\2\2\u01c3")
        buf.write("^\3\2\2\2\u01c4\u01c5\7+\2\2\u01c5`\3\2\2\2\u01c6\u01c7")
        buf.write("\7}\2\2\u01c7b\3\2\2\2\u01c8\u01c9\7\177\2\2\u01c9d\3")
        buf.write("\2\2\2\u01ca\u01cb\7]\2\2\u01cbf\3\2\2\2\u01cc\u01cd\7")
        buf.write("_\2\2\u01cdh\3\2\2\2\u01ce\u01cf\7=\2\2\u01cfj\3\2\2\2")
        buf.write("\u01d0\u01d1\7.\2\2\u01d1l\3\2\2\2\u01d2\u01d3\7<\2\2")
        buf.write("\u01d3n\3\2\2\2\u01d4\u01d5\7,\2\2\u01d5p\3\2\2\2\u01d6")
        buf.write("\u01d7\7\61\2\2\u01d7r\3\2\2\2\u01d8\u01d9\7-\2\2\u01d9")
        buf.write("t\3\2\2\2\u01da\u01db\7/\2\2\u01dbv\3\2\2\2\u01dc\u01dd")
        buf.write("\7>\2\2\u01dd\u01de\7@\2\2\u01dex\3\2\2\2\u01df\u01e0")
        buf.write("\7>\2\2\u01e0z\3\2\2\2\u01e1\u01e2\7>\2\2\u01e2\u01e3")
        buf.write("\7?\2\2\u01e3|\3\2\2\2\u01e4\u01e5\7@\2\2\u01e5~\3\2\2")
        buf.write("\2\u01e6\u01e7\7@\2\2\u01e7\u01e8\7?\2\2\u01e8\u0080\3")
        buf.write("\2\2\2\u01e9\u01ea\7?\2\2\u01ea\u0082\3\2\2\2\u01eb\u01ec")
        buf.write("\7<\2\2\u01ec\u01ed\7?\2\2\u01ed\u0084\3\2\2\2\u01ee\u01ef")
        buf.write("\13\2\2\2\u01ef\u01f0\bC\6\2\u01f0\u0086\3\2\2\2\35\2")
        buf.write("\u008a\u0091\u0099\u009c\u00a1\u00a7\u00ae\u00b0\u00b4")
        buf.write("\u00b9\u00bb\u00bf\u0168\u0174\u0176\u017e\u0180\u0186")
        buf.write("\u0188\u018d\u0195\u0197\u01a0\u01aa\u01b4\u01bf\7\b\2")
        buf.write("\2\3(\2\3)\3\3*\4\3C\5")
        return buf.getvalue()


class MPLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    WS = 1
    INTLIT = 2
    DDOT = 3
    REALLIT = 4
    BOOLEANLIT = 5
    INTTYPE = 6
    REALTYPE = 7
    BOOLEANTYPE = 8
    STRINGTYPE = 9
    ARRAYTYPE = 10
    OF = 11
    IF = 12
    ELSE = 13
    THEN = 14
    FOR = 15
    TO = 16
    DOWNTO = 17
    DO = 18
    WHILE = 19
    BEGIN = 20
    FUNCTION = 21
    PROCEDURE = 22
    VAR = 23
    END = 24
    WITH = 25
    TRUE = 26
    FALSE = 27
    BREAK = 28
    CONTINUE = 29
    RETURN = 30
    MOD = 31
    DIV = 32
    NOT = 33
    AND = 34
    OR = 35
    ID = 36
    UNCLOSE_STRING = 37
    ILLEGAL_ESCAPE = 38
    STRINGLIT = 39
    COMMENT = 40
    LB = 41
    RB = 42
    LP = 43
    RP = 44
    LS = 45
    RS = 46
    SEMI = 47
    COMMA = 48
    COLON = 49
    MULTOP = 50
    DIVOP = 51
    ADDOP = 52
    MINUSOP = 53
    NEOP = 54
    LTOP = 55
    LEOP = 56
    GTOP = 57
    GEOP = 58
    EQOP = 59
    ASOP = 60
    ERROR_CHAR = 61

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'..'", "'('", "')'", "'{'", "'}'", "'['", "']'", "';'", "','", 
            "':'", "'*'", "'/'", "'+'", "'-'", "'<>'", "'<'", "'<='", "'>'", 
            "'>='", "'='", "':='" ]

    symbolicNames = [ "<INVALID>",
            "WS", "INTLIT", "DDOT", "REALLIT", "BOOLEANLIT", "INTTYPE", 
            "REALTYPE", "BOOLEANTYPE", "STRINGTYPE", "ARRAYTYPE", "OF", 
            "IF", "ELSE", "THEN", "FOR", "TO", "DOWNTO", "DO", "WHILE", 
            "BEGIN", "FUNCTION", "PROCEDURE", "VAR", "END", "WITH", "TRUE", 
            "FALSE", "BREAK", "CONTINUE", "RETURN", "MOD", "DIV", "NOT", 
            "AND", "OR", "ID", "UNCLOSE_STRING", "ILLEGAL_ESCAPE", "STRINGLIT", 
            "COMMENT", "LB", "RB", "LP", "RP", "LS", "RS", "SEMI", "COMMA", 
            "COLON", "MULTOP", "DIVOP", "ADDOP", "MINUSOP", "NEOP", "LTOP", 
            "LEOP", "GTOP", "GEOP", "EQOP", "ASOP", "ERROR_CHAR" ]

    ruleNames = [ "WS", "INTLIT", "DDOT", "REALLIT", "BOOLEANLIT", "INTTYPE", 
                  "REALTYPE", "BOOLEANTYPE", "STRINGTYPE", "ARRAYTYPE", 
                  "OF", "IF", "ELSE", "THEN", "FOR", "TO", "DOWNTO", "DO", 
                  "WHILE", "BEGIN", "FUNCTION", "PROCEDURE", "VAR", "END", 
                  "WITH", "TRUE", "FALSE", "BREAK", "CONTINUE", "RETURN", 
                  "MOD", "DIV", "NOT", "AND", "OR", "ID", "ESC_CHAR", "ILL_ESC_CHAR", 
                  "UNCLOSE_STRING", "ILLEGAL_ESCAPE", "STRINGLIT", "COMMENT", 
                  "BlockComment_first", "BlockComment_second", "LineComment", 
                  "LB", "RB", "LP", "RP", "LS", "RS", "SEMI", "COMMA", "COLON", 
                  "MULTOP", "DIVOP", "ADDOP", "MINUSOP", "NEOP", "LTOP", 
                  "LEOP", "GTOP", "GEOP", "EQOP", "ASOP", "ERROR_CHAR" ]

    grammarFileName = "MP.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


    def action(self, localctx:RuleContext, ruleIndex:int, actionIndex:int):
        if self._actions is None:
            actions = dict()
            actions[38] = self.UNCLOSE_STRING_action 
            actions[39] = self.ILLEGAL_ESCAPE_action 
            actions[40] = self.STRINGLIT_action 
            actions[65] = self.ERROR_CHAR_action 
            self._actions = actions
        action = self._actions.get(ruleIndex, None)
        if action is not None:
            action(localctx, actionIndex)
        else:
            raise Exception("No registered action for:" + str(ruleIndex))

    def UNCLOSE_STRING_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 0:

                        tmp = self.text
                        tmp = tmp[1:]
                        raise UncloseString(tmp)
                    
     

    def ILLEGAL_ESCAPE_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 1:

                        tmp = self.text
                        for index in range(1 ,len(tmp) - 2):
                            if tmp[index] == '\\':
                                legal_char = tmp[index + 1]
                                if  legal_char == '\\' or legal_char == 'b' or legal_char == 'f' or legal_char == 'r' or legal_char == 'n' or legal_char == 't' or legal_char == '\'' or legal_char == '\"' :
                                    index += 1
                                    continue
                                else:
                                    break
                        tmp = tmp[1:index + 2]
                        raise IllegalEscape(tmp)
                    
     

    def STRINGLIT_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 2:

                        tmp = self.text
                        tmp = tmp[1:-1]
                        self.text = tmp
                    
     

    def ERROR_CHAR_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 3:

                    raise ErrorToken(self.text)
                
     


