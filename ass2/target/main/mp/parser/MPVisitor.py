# Generated from main/mp/parser/MP.g4 by ANTLR 4.7.1
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .MPParser import MPParser
else:
    from MPParser import MPParser

# This class defines a complete generic visitor for a parse tree produced by MPParser.

class MPVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by MPParser#program.
    def visitProgram(self, ctx:MPParser.ProgramContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#decl.
    def visitDecl(self, ctx:MPParser.DeclContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#vardecl.
    def visitVardecl(self, ctx:MPParser.VardeclContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#funcdecl.
    def visitFuncdecl(self, ctx:MPParser.FuncdeclContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#procdecl.
    def visitProcdecl(self, ctx:MPParser.ProcdeclContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#para_list.
    def visitPara_list(self, ctx:MPParser.Para_listContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#para_decl.
    def visitPara_decl(self, ctx:MPParser.Para_declContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#mptype.
    def visitMptype(self, ctx:MPParser.MptypeContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#primitive_type.
    def visitPrimitive_type(self, ctx:MPParser.Primitive_typeContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#array_type.
    def visitArray_type(self, ctx:MPParser.Array_typeContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#compound_stmt.
    def visitCompound_stmt(self, ctx:MPParser.Compound_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#stmt.
    def visitStmt(self, ctx:MPParser.StmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#match_if.
    def visitMatch_if(self, ctx:MPParser.Match_ifContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#unmatch_if.
    def visitUnmatch_if(self, ctx:MPParser.Unmatch_ifContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#whiledo_stmt.
    def visitWhiledo_stmt(self, ctx:MPParser.Whiledo_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#for_stmt.
    def visitFor_stmt(self, ctx:MPParser.For_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#with_stmt.
    def visitWith_stmt(self, ctx:MPParser.With_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#other.
    def visitOther(self, ctx:MPParser.OtherContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#assign_stmt.
    def visitAssign_stmt(self, ctx:MPParser.Assign_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#break_stmt.
    def visitBreak_stmt(self, ctx:MPParser.Break_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#continue_stmt.
    def visitContinue_stmt(self, ctx:MPParser.Continue_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#return_stmt.
    def visitReturn_stmt(self, ctx:MPParser.Return_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#list_expr.
    def visitList_expr(self, ctx:MPParser.List_exprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#list_vardecl.
    def visitList_vardecl(self, ctx:MPParser.List_vardeclContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#call_stmt.
    def visitCall_stmt(self, ctx:MPParser.Call_stmtContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#expr.
    def visitExpr(self, ctx:MPParser.ExprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#expr1.
    def visitExpr1(self, ctx:MPParser.Expr1Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#expr2.
    def visitExpr2(self, ctx:MPParser.Expr2Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#expr3.
    def visitExpr3(self, ctx:MPParser.Expr3Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#expr4.
    def visitExpr4(self, ctx:MPParser.Expr4Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#expr5.
    def visitExpr5(self, ctx:MPParser.Expr5Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#expr6.
    def visitExpr6(self, ctx:MPParser.Expr6Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#function_call.
    def visitFunction_call(self, ctx:MPParser.Function_callContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#index_expr.
    def visitIndex_expr(self, ctx:MPParser.Index_exprContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#inttype.
    def visitInttype(self, ctx:MPParser.InttypeContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#booltype.
    def visitBooltype(self, ctx:MPParser.BooltypeContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#realtype.
    def visitRealtype(self, ctx:MPParser.RealtypeContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MPParser#stringtype.
    def visitStringtype(self, ctx:MPParser.StringtypeContext):
        return self.visitChildren(ctx)



del MPParser