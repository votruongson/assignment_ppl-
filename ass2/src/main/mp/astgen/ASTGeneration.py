from MPVisitor import MPVisitor
from MPParser import MPParser
from AST import *
import functools

class ASTGeneration(MPVisitor):
    def visitProgram(self,ctx:MPParser.ProgramContext):
        listdecl =[]
        for x in ctx.decl():
            listdecl += self.visit(x)
        return Program(listdecl)

    def visitDecl(self,ctx:MPParser.DeclContext):
        return self.visit(ctx.getChild(0))

    def visitVardecl(self, ctx: MPParser.VardeclContext):
        listVariable = []
        for decl in ctx.para_decl():
            for decl_id in decl.ID():
                listVariable.append(VarDecl(Id(decl_id.getText()), self.visit(decl.mptype())))
        return listVariable

    def visitFuncdecl(self,ctx:MPParser.FuncdeclContext):
        name = Id(ctx.ID().getText())
        param = []
        local = self.visit(ctx.vardecl()) if ctx.vardecl() else []
        cpstmt = self.visit(ctx.compound_stmt())
        returnType = self.visit(ctx.mptype())
        if ctx.para_list():
            param = self.visit(ctx.para_list())
            if not(isinstance(param, list)):
                param = [param]
        return [FuncDecl(name,param,local,cpstmt,returnType)]

    def visitProcdecl(self,ctx:MPParser.ProcdeclContext):
        name = Id(ctx.ID().getText())
        para = self.visit(ctx.para_list()) if ctx.para_list() else []
        local = self.visit(ctx.vardecl()) if ctx.vardecl() else []
        cpstmt = self.visit(ctx.compound_stmt())
        return [FuncDecl(name,para,local,cpstmt)]

    def visitPara_list(self,ctx:MPParser.Para_listContext):
        listVariable = []
        for decl in ctx.para_decl():
            for decl_id in decl.ID():
                listVariable.append(VarDecl(Id(decl_id.getText()), self.visit(decl.mptype())))
        return listVariable

    def visitCompound_stmt(self,ctx:MPParser.Compound_stmtContext):
        listStmt = []
        for stmt in ctx.stmt():
            stmt_visit = self.visit(stmt)
            if isinstance(stmt_visit,list):
                listStmt += stmt_visit
            else:
                listStmt.append(stmt_visit)
        return listStmt

    def visitStmt(self,ctx:MPParser.StmtContext):
        return self.visit(ctx.getChild(0))

    def visitMatch_if(self,ctx:MPParser.Match_ifContext):
        if ctx.getChildCount() == 1:
            return self.visit(ctx.other())
        else:
            expr = self.visit(ctx.expr())
            thenStmt = self.visit(ctx.match_if(0))
            elseStmt = self.visit(ctx.match_if(1))
            if not(isinstance(thenStmt,list)):
                thenStmt = [thenStmt]
            if not(isinstance(elseStmt,list)):
                elseStmt = [elseStmt]
            return If(expr,thenStmt,elseStmt)

    def visitUnmatch_if(self,ctx:MPParser.Unmatch_ifContext):
        if ctx.getChildCount() == 1:
            return self.visit(ctx.other())
        elif ctx.getChildCount() == 4:
            expr = self.visit(ctx.expr())
            thenStmt = self.visit(ctx.stmt())
            if not(isinstance(thenStmt,list)):
                thenStmt = [thenStmt]
            return If(expr,thenStmt,[])
        else:
            expr = self.visit(ctx.expr())
            thenStmt = self.visit(ctx.match_if())
            elseStmt = self.visit(ctx.unmatch_if())
            if not(isinstance(thenStmt,list)):
                thenStmt = [thenStmt]
            if not(isinstance(elseStmt,list)):
                elseStmt = [elseStmt]
            return If(expr,thenStmt,elseStmt)

    def visitOther(self,ctx:MPParser.OtherContext):
            return self.visit(ctx.getChild(0))


    def visitAssign_stmt(self,ctx:MPParser.Assign_stmtContext):
        assignlist = []
        size = len(ctx.expr5())
        for idx, val in enumerate(ctx.expr5()):
            if size > idx + 1:
                assignlist.insert(0, Assign(self.visit(val),self.visit(ctx.expr5(idx + 1))))
            else :
                assignlist.insert(0, Assign(self.visit(val), self.visit(ctx.expr())))
        return assignlist

    def visitWhiledo_stmt(self,ctx:MPParser.Whiledo_stmtContext):
        expr = self.visit(ctx.expr())
        list_stmt = self.visit(ctx.stmt())
        if not (isinstance(list_stmt,list)):
            list_stmt = [list_stmt]
        return While(expr,list_stmt)
    
    def visitFor_stmt(self,ctx:MPParser.For_stmtContext):
        name = Id(ctx.ID().getText()) 
        expr1 = self.visit(ctx.expr(0))
        expr2 = self.visit(ctx.expr(1))
        up = True if ctx.TO() else False
        list_stmt = self.visit(ctx.stmt())
        if not (isinstance(list_stmt,list)):
            list_stmt = [list_stmt]
        return For(name,expr1,expr2,up,list_stmt)

    def visitBreak_stmt(self,ctx:MPParser.Break_stmtContext):
        return Break()

    def visitContinue_stmt(self,ctx:MPParser.Continue_stmtContext):
        return Continue()

    def visitReturn_stmt(self,ctx:MPParser.Return_stmtContext):
        return_stmt = self.visit(ctx.expr()) if ctx.expr() else None 
        return Return(return_stmt)
        
    def visitCall_stmt(self,ctx:MPParser.Call_stmtContext):
        method = Id(ctx.ID().getText())
        para = self.visit(ctx.list_expr()) if ctx.list_expr() else []
        return CallStmt(method,para)

    def visitList_expr(self,ctx:MPParser.List_exprContext):
        return [self.visit(x) for x in ctx.expr()]

    def visitWith_stmt(self,ctx:MPParser.With_stmtContext):
        decl = self.visit(ctx.list_vardecl())
        stmt = self.visit(ctx.stmt())
        return With(decl,stmt)

    def visitList_vardecl(self,ctx:MPParser.List_vardeclContext):
        listVariable = []
        for decl in ctx.para_decl():
            for decl_id in decl.ID():
               listVariable.append(VarDecl(Id(decl_id.getText()), self.visit(decl.mptype())))
        return listVariable 


    def visitExpr(self,ctx:MPParser.ExprContext):
        if ctx.getChildCount() == 1 :
            return self.visit(ctx.expr1())
        elif ctx.AND() :
            return BinaryOp("andthen",self.visit(ctx.expr1()),self.visit(ctx.expr()))
        else :
            return BinaryOp('orelse',self.visit(ctx.expr1()),self.visit(ctx.expr()))

    # def visitExpr1(self,ctx:MPParser.Expr1Context):
    #     exp2_a = self.visit(ctx.expr2(0))
    #     exp2_b = self.visit(ctx.expr2(1)) if ctx.expr2(1) else []
    #     return exp2_a if ctx.getChildCount() == 1 else  BinaryOp(str(ctx.getChild(1)) ,exp2_a ,exp2_b )
    def visitExpr1(self,ctx:MPParser.Expr1Context):
        if ctx.getChildCount() == 1 :
            return self.visit(ctx.getChild(0))
        else :
            return BinaryOp(str(ctx.getChild(1)),self.visit(ctx.expr2(0)),self.visit(ctx.expr2(1)))
    def visitExpr2(self,ctx:MPParser.Expr2Context):
        if ctx.getChildCount() == 1 :
            return self.visit(ctx.getChild(0))
        else :
            return BinaryOp(str(ctx.getChild(1)),self.visit(ctx.expr2()),self.visit(ctx.expr3()))

    def visitExpr3(self,ctx:MPParser.Expr3Context):
        if ctx.getChildCount() == 1 :
            return self.visit(ctx.getChild(0))
        else :
            return BinaryOp(str(ctx.getChild(1)),self.visit(ctx.expr3()),self.visit(ctx.expr4()))
        
    def visitExpr4(self,ctx:MPParser.Expr4Context):
        if ctx.getChildCount() == 1 :
            return self.visit(ctx.getChild(0))
        else :
            return UnaryOp(str(ctx.getChild(0)),self.visit(ctx.expr4()))

    def visitExpr5(self,ctx:MPParser.Expr5Context):
        if ctx.getChildCount() == 1 :
            return self.visit(ctx.getChild(0))
        else :
            return ArrayCell(self.visit(ctx.expr6()),self.visit(ctx.expr()))


    def visitExpr6(self,ctx:MPParser.Expr6Context):
        if ctx.INTLIT() :
            return IntLiteral(int(ctx.INTLIT().getText()))
        elif ctx.BOOLLIT() :
            boolList = ctx.BOOLLIT().getText()
            if boolList.upper() == 'TRUE' :
                return BooleanLiteral(True)
            else :
                return BooleanLiteral(False)
        elif ctx.STRINGLIT() : 
            return StringLiteral(ctx.STRINGLIT().getText())
        elif ctx.REALLIT() :
            return FloatLiteral(float(ctx.REALLIT().getText()))
        elif ctx.ID() :
            return Id(ctx.ID().getText())
        elif ctx.expr() :
            return self.visit(ctx.expr())
        else :
            return self.visit(ctx.function_call())

    def visitFunction_call(self,ctx:MPParser.Function_callContext):
        para = self.visit(ctx.list_expr()) if ctx.list_expr() else []
        return CallExpr(Id(ctx.ID().getText()),para)



    def visitMptype(self,ctx:MPParser.MptypeContext):
        return self.visit(ctx.getChild(0))

    def visitPrimitive_type(self, ctx: MPParser.Primitive_typeContext):
        if ctx.inttype():
            return IntType()
        elif ctx.realtype():
            return FloatType()
        elif ctx.booltype():
            return BoolType()
        else:
            return StringType()
    def visitArray_type(self,ctx:MPParser.Array_typeContext):
        lower = self.visit(ctx.expr(0))
        upper = self.visit(ctx.expr(1))
        eleType = self.visit(ctx.primitive_type())
        return ArrayType(lower,upper,eleType)

