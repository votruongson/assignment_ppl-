import unittest
from TestUtils import TestAST
from AST import *

class ASTGenSuite(unittest.TestCase):
    def test_simple_vardeclare(self):
        """test simple vardeclare """
        input = """
            var a, b: integer; c: real;
            """
        expect = str(Program([VarDecl(Id("a"), IntType()), VarDecl(Id("b"), IntType()), VarDecl(Id("c"), FloatType())]))
        self.assertTrue(TestAST.test(input,expect,300))
    def test_ArrayType(self):
        """test ArrayType """
        input = """
            var a: array [1 .. 5] of integer;
            """
        expect = str(Program([VarDecl(Id("a"), ArrayType(1, 5, IntType()))]))
        self.assertTrue(TestAST.test(input,expect,301))
    def test_many_vardeclare(self):
        """test ArrayType """
        input = """
            var a, b: integer; c: real;
            var d, e: string; f: array [1 .. 5] of integer;
            var x: boolean;
            """
        expect = str(Program([VarDecl(Id("a"), IntType()), VarDecl(Id("b"), IntType()), VarDecl(Id("c"), FloatType()),
                    VarDecl(Id("d"), StringType()), VarDecl(Id("e"), StringType()), VarDecl(Id("f"), ArrayType(1, 5, IntType())),
                    VarDecl(Id("x"), BoolType())]))
        self.assertTrue(TestAST.test(input,expect,302))
    def test_simple_procedure(self):
        """test simple procedure """
        input = """
            procedure main(); 
                begin 
                end
            """
        expect = str(Program([FuncDecl(Id("main"),[],[],[])]))
        self.assertTrue(TestAST.test(input,expect,303))
    def test_simple_function(self):
        """test simple function """
        input = """
            function main():integer; 
                begin 
                end
            """
        expect = str(Program([FuncDecl(Id("main"),[],[],[],IntType())]))
        self.assertTrue(TestAST.test(input,expect,304))
    def test_function_with_parameter(self):
        """test function with parameter """
        input = """
            function main( a, b: integer; c: real ):integer; 
                begin 
                end
            """
        expect = str(Program([FuncDecl(Id("main"),[VarDecl(Id("a"), IntType()), VarDecl(Id("b"), IntType()), VarDecl(Id("c"), FloatType())],[],[],IntType())]))
        self.assertTrue(TestAST.test(input,expect,305))

    def test_function_with_local(self):
        """test simple function """
        input = """
            function main():integer;
                var d, e: string; f: real; 
                begin 
                end
            """
        expect = str(Program([FuncDecl(Id("main"),
                    [],
                    [VarDecl(Id("d"), StringType()), VarDecl(Id("e"), StringType()), VarDecl(Id("f"), FloatType())],
                    [],
                    IntType())]))
        self.assertTrue(TestAST.test(input,expect,306))
    
    def test_function_with_body(self):
        """test simple function """
        input = """
            function main():integer;
                begin 
                    putIntLn(4);
                end
            """
        expect = str(Program([FuncDecl(Id("main"),[],[],[CallStmt(Id("putIntLn"),[IntLiteral(4)])],IntType())]))
        self.assertTrue(TestAST.test(input,expect,307))

    def test_function_with_full_declare(self):
        """test simple function """
        input = """
            function main( a, b: integer; c: real ):integer;
                var d, e: string; f: real; 
                begin
                    putIntLn(4);
                end
            """
        expect = str(Program([FuncDecl(Id("main"),
                    [VarDecl(Id("a"), IntType()), VarDecl(Id("b"), IntType()), VarDecl(Id("c"), FloatType())],
                    [VarDecl(Id("d"), StringType()), VarDecl(Id("e"), StringType()), VarDecl(Id("f"), FloatType())],
                    [CallStmt(Id("putIntLn"),[IntLiteral(4)])],
                    IntType())]))
        self.assertTrue(TestAST.test(input,expect,308))
    
    def test_multiassign(self):
        """test multiassign """
        input = """
            procedure main(); 
                begin
                    a := b := c := x + 15;
                end
            """
        expect = str(Program([FuncDecl(Id("main"),[],[],[Assign(Id("a"), Assign(Id("b"), Assign(Id("c"), BinaryOp("+",Id("x"),IntLiteral(15)))))])]))
        self.assertTrue(TestAST.test(input,expect,309))
    
    def test_cpStmt_in_cpStmt(self):
        """test simple function """
        input = """
            function main():integer;
                begin
                    begin
                        putIntLn(4);
                    end
                end
            """
        expect = str(Program([FuncDecl(Id("main"),
                    [],
                    [],
                    [CallStmt(Id("putIntLn"),[IntLiteral(4)])],
                    IntType())]))
        self.assertTrue(TestAST.test(input,expect,310))

    def test_cpStmt_in_cpStmt2(self):
        """test simple function """
        input = """
            function main():integer;
                begin
                    foo1(1);
                    foo2(2);
                    begin
                        putIntLn(4);
                    end
                end
            """
        expect = str(Program([FuncDecl(Id("main"),
                    [],
                    [],
                    [CallStmt(Id("foo1"),[IntLiteral(1)]), CallStmt(Id("foo2"),[IntLiteral(2)]), CallStmt(Id("putIntLn"),[IntLiteral(4)])],
                    IntType())]))
        self.assertTrue(TestAST.test(input,expect,311))

    def test_cpStmt_in_cpStmt3(self):
        """test simple function """
        input = """
            function main():integer;
                begin
                    foo1(1);
                    foo2(2);
                    begin
                        foo3(3);
                            begin
                                foo4(4);
                            end
                        foo5(5);
                    end
                end
            """
        expect = str(Program([FuncDecl(Id("main"),
                    [],
                    [],
                    [CallStmt(Id("foo1"),[IntLiteral(1)]), 
                    CallStmt(Id("foo2"),[IntLiteral(2)]),
                    CallStmt(Id("foo3"),[IntLiteral(3)]),
                    CallStmt(Id("foo4"),[IntLiteral(4)]),
                    CallStmt(Id("foo5"),[IntLiteral(5)])],
                    IntType())]))
        self.assertTrue(TestAST.test(input,expect,312))

    def test_returnStmt(self):
        """test simple function """
        input = """
            function main():integer; 
                begin
                    return a;
                end
            """
        expect = str(Program([FuncDecl(Id("main"),[],[],[Return(Id("a"))],IntType())]))
        self.assertTrue(TestAST.test(input,expect,313))

    def test_BoolLiteral(self):
        """test simple function """
        input = """
            function main():integer; 
                begin
                    a := True;
                end
            """
        expect = str(Program([FuncDecl(Id("main"),[],[],[Assign(Id("a"), BooleanLiteral(True))],IntType())]))
        self.assertTrue(TestAST.test(input,expect,313))

    def test_StringLiteral(self):
        """test simple function """
        input = """
            function main():integer; 
                begin
                    a := "Hom nay" + " ngay chu nhat";
                end
            """
        expect = str(Program([FuncDecl(Id("main"),[],[],[Assign(Id("a"), BinaryOp("+", StringLiteral("Hom nay"), StringLiteral(" ngay chu nhat")))],IntType())]))
        self.assertTrue(TestAST.test(input,expect,314))

    def test_simple_expression(self):
        """test simple function """
        input = """
            function main():integer; 
                begin
                    a := 4*b + c;
                end
            """
        expect = str(Program([FuncDecl(Id("main"),[],[],[Assign(Id("a"), BinaryOp("+",BinaryOp("*",IntLiteral(4),Id("b")),Id("c")))],IntType())]))
        self.assertTrue(TestAST.test(input,expect,315))

    def test_andthen_orelse(self):
        """test simple function """
        input = """
            function main():integer; 
                begin
                    a := b or else 2 ;
                end
            """
        expect = str(Program([FuncDecl(Id("main"),[],[],[Assign(Id("a"), BinaryOp("orelse",BinaryOp("andthen",Id("b"),IntLiteral(2)),Id("d")))],IntType())]))
        self.assertTrue(TestAST.test(input,expect,316))

    # def test_lhs_with_arraycell(self):
    #     """test lhs with arraycell """
    #     input = """
    #         procedure main(); 
    #             begin
    #                 a()[2] := x + 15;
    #             end
    #         """
    #     expect = str(Program([FuncDecl(Id("main"),[],[],[Assign(Id("a"), BinaryOp("+",Id("x"),IntLiteral(15)))])]))
    #     self.assertTrue(TestAST.test(input,expect,309))

    # def test_lhs_with_arraycell(self):
    #     """test lhs with arraycell """
    #     input = """
    #         procedure main(); 
    #             begin
    #                 a[b[c+4]];
    #             end
    #         """
    #     expect = str(Program([FuncDecl(Id("main"),[],[],[Assign(Id("a"), BinaryOp("+",Id("x"),IntLiteral(15)))])]))
    #     self.assertTrue(TestAST.test(input,expect,309))
    
    # def test_call_without_parameter(self):
    #     """More complex program"""
    #     input = """procedure main (); begin
    #         getIntLn();
    #     end
    #     function foo ():INTEGER; begin
    #         putIntLn(4);
    #     end"""
    #     expect = str(Program([
    #             FuncDecl(Id("main"),[],[],[CallStmt(Id("getIntLn"),[])]),
    #             FuncDecl(Id("foo"),[],[],[CallStmt(Id("putIntLn"),[IntLiteral(4)])],IntType())]))
    #     self.assertTrue(TestAST.test(input,expect,398))
    # def test_program13(self):
    #     input = """
    #             procedure main(d:integer) ;
    #             var b, c, d:integer; e,f: real;               
    #             begin 
    #                 g := q;
    #                 begin
    #                     f(2) := a * ef/2;
    #                     a := g[a[f[3]]] + x/y;
    #                 end
    #             end
    #             """
    #     expect = str(Program([
    #             FuncDecl(Id("main"),[],[],[CallStmt(Id("getIntLn"),[])]),
    #             FuncDecl(Id("foo"),[],[],[CallStmt(Id("putIntLn"),[IntLiteral(4)])],IntType())]))
    #     self.assertTrue(TestAST.test(input,expect,399))
   
    def test_andthen_orelse1(self):
        """test simple function """
        input = """
            function main():integer; 
                begin
                   if (a > 2) then printf(a) else printf(a) ;  
                end
            """
        expect = str(Program([FuncDecl(Id("main"),[],[],[Assign(Id("a"), BinaryOp("orelse",BinaryOp("andthen",Id("b"),IntLiteral(2)),Id("d")))],IntType())]))
        self.assertTrue(TestAST.test(input,expect,340))