# Generated from main/mp/parser/MP.g4 by ANTLR 4.7.1
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


from lexererr import *


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2B")
        buf.write("\u01fb\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23")
        buf.write("\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30")
        buf.write("\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36")
        buf.write("\t\36\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%")
        buf.write("\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4,\t,\4-\t-\4.")
        buf.write("\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64")
        buf.write("\t\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:")
        buf.write("\4;\t;\4<\t<\4=\t=\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\t")
        buf.write("C\3\2\6\2\u0089\n\2\r\2\16\2\u008a\3\3\3\3\3\3\5\3\u0090")
        buf.write("\n\3\3\3\5\3\u0093\n\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4")
        buf.write("\3\4\3\4\5\4\u009f\n\4\3\5\3\5\5\5\u00a3\n\5\3\6\3\6\3")
        buf.write("\6\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t")
        buf.write("\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\13")
        buf.write("\3\13\3\13\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f")
        buf.write("\3\r\3\r\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\16\3\16")
        buf.write("\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\20\3\20\3\20\3\21")
        buf.write("\3\21\3\21\3\21\3\21\3\21\3\21\3\22\3\22\3\22\3\23\3\23")
        buf.write("\3\23\3\24\3\24\3\24\3\24\3\24\3\25\3\25\3\25\3\25\3\25")
        buf.write("\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\27\3\27\3\27\3\27")
        buf.write("\3\27\3\27\3\30\3\30\3\30\3\30\3\30\3\30\3\31\3\31\3\31")
        buf.write("\3\31\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\32\3\33")
        buf.write("\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\34\3\34")
        buf.write("\3\34\3\35\3\35\3\35\3\35\3\36\3\36\3\36\3\36\3\36\3\37")
        buf.write("\3\37\3\37\3\37\3\37\3 \3 \3 \3 \3 \3 \3!\3!\3\"\3\"\3")
        buf.write("#\3#\3$\3$\3%\3%\3%\3%\3&\3&\3&\3&\3\'\3\'\3\'\3\'\3(")
        buf.write("\3(\3(\3)\3)\3)\3*\3*\3+\3+\3+\3,\3,\3-\3-\3-\3.\3.\3")
        buf.write("/\3/\3/\3\60\3\60\3\61\3\61\3\62\3\62\3\63\3\63\3\64\3")
        buf.write("\64\3\65\3\65\3\66\3\66\3\67\3\67\7\67\u0175\n\67\f\67")
        buf.write("\16\67\u0178\13\67\38\68\u017b\n8\r8\168\u017c\38\38\3")
        buf.write("9\39\39\39\79\u0185\n9\f9\169\u0188\139\39\39\39\39\3")
        buf.write("9\3:\3:\7:\u0191\n:\f:\16:\u0194\13:\3:\3:\3:\3:\3;\3")
        buf.write(";\3;\3;\7;\u019e\n;\f;\16;\u01a1\13;\3;\3;\3<\3<\3<\3")
        buf.write("<\7<\u01a9\n<\f<\16<\u01ac\13<\3<\3<\3<\3<\3<\3=\3=\7")
        buf.write("=\u01b5\n=\f=\16=\u01b8\13=\3=\3=\3=\3=\3>\3>\3>\3>\7")
        buf.write(">\u01c2\n>\f>\16>\u01c5\13>\3>\3>\3?\3?\3?\3@\3@\3@\7")
        buf.write("@\u01cf\n@\f@\16@\u01d2\13@\3@\3@\3@\3A\3A\3A\7A\u01da")
        buf.write("\nA\fA\16A\u01dd\13A\3A\5A\u01e0\nA\3A\3A\3B\3B\3B\7B")
        buf.write("\u01e7\nB\fB\16B\u01ea\13B\3B\3B\3B\3B\3B\3B\7B\u01f2")
        buf.write("\nB\fB\16B\u01f5\13B\3B\3B\3C\3C\3C\7\u0186\u0192\u01aa")
        buf.write("\u01b6\u01e8\2D\3\3\5\4\7\2\t\5\13\6\r\7\17\b\21\t\23")
        buf.write("\n\25\13\27\f\31\r\33\16\35\17\37\20!\21#\22%\23\'\24")
        buf.write(")\25+\26-\27/\30\61\31\63\32\65\33\67\349\35;\36=\37?")
        buf.write(" A!C\"E#G$I%K&M\'O(Q)S*U+W,Y-[.]/_\60a\61c\62e\63g\64")
        buf.write("i\65k\66m\67o8q9s:u;w<y={>}\2\177?\u0081@\u0083A\u0085")
        buf.write("B\3\2$\3\2\62;\4\2GGgg\3\2//\4\2XXxx\4\2CCcc\4\2TTtt\4")
        buf.write("\2NNnn\4\2DDdd\4\2QQqq\4\2PPpp\4\2KKkk\4\2VVvv\4\2IIi")
        buf.write("i\4\2UUuu\4\2[[{{\4\2MMmm\4\2EEee\4\2WWww\4\2HHhh\4\2")
        buf.write("FFff\4\2YYyy\4\2JJjj\4\2RRrr\4\2OOoo\5\2C\\aac|\6\2\62")
        buf.write(";C\\aac|\5\2\13\f\16\17\"\"\4\2\f\f\17\17\n\2$$))^^dd")
        buf.write("hhppttvv\6\2\f\f\17\17$$^^\5\2\f\f\17\17$$\3\3\f\f\5\2")
        buf.write("\f\f\17\17^^\3\2^^\2\u020f\2\3\3\2\2\2\2\5\3\2\2\2\2\t")
        buf.write("\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3")
        buf.write("\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2")
        buf.write("\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2")
        buf.write("\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2")
        buf.write("\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65")
        buf.write("\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2")
        buf.write("\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2")
        buf.write("\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2")
        buf.write("\2\2\2S\3\2\2\2\2U\3\2\2\2\2W\3\2\2\2\2Y\3\2\2\2\2[\3")
        buf.write("\2\2\2\2]\3\2\2\2\2_\3\2\2\2\2a\3\2\2\2\2c\3\2\2\2\2e")
        buf.write("\3\2\2\2\2g\3\2\2\2\2i\3\2\2\2\2k\3\2\2\2\2m\3\2\2\2\2")
        buf.write("o\3\2\2\2\2q\3\2\2\2\2s\3\2\2\2\2u\3\2\2\2\2w\3\2\2\2")
        buf.write("\2y\3\2\2\2\2{\3\2\2\2\2\177\3\2\2\2\2\u0081\3\2\2\2\2")
        buf.write("\u0083\3\2\2\2\2\u0085\3\2\2\2\3\u0088\3\2\2\2\5\u008c")
        buf.write("\3\2\2\2\7\u009e\3\2\2\2\t\u00a2\3\2\2\2\13\u00a4\3\2")
        buf.write("\2\2\r\u00a7\3\2\2\2\17\u00ab\3\2\2\2\21\u00b0\3\2\2\2")
        buf.write("\23\u00b8\3\2\2\2\25\u00c0\3\2\2\2\27\u00c7\3\2\2\2\31")
        buf.write("\u00cd\3\2\2\2\33\u00d3\3\2\2\2\35\u00dc\3\2\2\2\37\u00e0")
        buf.write("\3\2\2\2!\u00e3\3\2\2\2#\u00ea\3\2\2\2%\u00ed\3\2\2\2")
        buf.write("\'\u00f0\3\2\2\2)\u00f5\3\2\2\2+\u00fa\3\2\2\2-\u0101")
        buf.write("\3\2\2\2/\u0107\3\2\2\2\61\u010d\3\2\2\2\63\u0111\3\2")
        buf.write("\2\2\65\u011a\3\2\2\2\67\u0124\3\2\2\29\u0127\3\2\2\2")
        buf.write(";\u012b\3\2\2\2=\u0130\3\2\2\2?\u0135\3\2\2\2A\u013b\3")
        buf.write("\2\2\2C\u013d\3\2\2\2E\u013f\3\2\2\2G\u0141\3\2\2\2I\u0143")
        buf.write("\3\2\2\2K\u0147\3\2\2\2M\u014b\3\2\2\2O\u014f\3\2\2\2")
        buf.write("Q\u0152\3\2\2\2S\u0155\3\2\2\2U\u0157\3\2\2\2W\u015a\3")
        buf.write("\2\2\2Y\u015c\3\2\2\2[\u015f\3\2\2\2]\u0161\3\2\2\2_\u0164")
        buf.write("\3\2\2\2a\u0166\3\2\2\2c\u0168\3\2\2\2e\u016a\3\2\2\2")
        buf.write("g\u016c\3\2\2\2i\u016e\3\2\2\2k\u0170\3\2\2\2m\u0172\3")
        buf.write("\2\2\2o\u017a\3\2\2\2q\u0180\3\2\2\2s\u018e\3\2\2\2u\u0199")
        buf.write("\3\2\2\2w\u01a4\3\2\2\2y\u01b2\3\2\2\2{\u01bd\3\2\2\2")
        buf.write("}\u01c8\3\2\2\2\177\u01cb\3\2\2\2\u0081\u01d6\3\2\2\2")
        buf.write("\u0083\u01e3\3\2\2\2\u0085\u01f8\3\2\2\2\u0087\u0089\t")
        buf.write("\2\2\2\u0088\u0087\3\2\2\2\u0089\u008a\3\2\2\2\u008a\u0088")
        buf.write("\3\2\2\2\u008a\u008b\3\2\2\2\u008b\4\3\2\2\2\u008c\u0092")
        buf.write("\5\7\4\2\u008d\u008f\t\3\2\2\u008e\u0090\t\4\2\2\u008f")
        buf.write("\u008e\3\2\2\2\u008f\u0090\3\2\2\2\u0090\u0091\3\2\2\2")
        buf.write("\u0091\u0093\5\3\2\2\u0092\u008d\3\2\2\2\u0092\u0093\3")
        buf.write("\2\2\2\u0093\6\3\2\2\2\u0094\u0095\5\3\2\2\u0095\u0096")
        buf.write("\7\60\2\2\u0096\u009f\3\2\2\2\u0097\u0098\7\60\2\2\u0098")
        buf.write("\u009f\5\3\2\2\u0099\u009a\5\3\2\2\u009a\u009b\7\60\2")
        buf.write("\2\u009b\u009c\5\3\2\2\u009c\u009f\3\2\2\2\u009d\u009f")
        buf.write("\5\3\2\2\u009e\u0094\3\2\2\2\u009e\u0097\3\2\2\2\u009e")
        buf.write("\u0099\3\2\2\2\u009e\u009d\3\2\2\2\u009f\b\3\2\2\2\u00a0")
        buf.write("\u00a3\5=\37\2\u00a1\u00a3\5? \2\u00a2\u00a0\3\2\2\2\u00a2")
        buf.write("\u00a1\3\2\2\2\u00a3\n\3\2\2\2\u00a4\u00a5\7\60\2\2\u00a5")
        buf.write("\u00a6\7\60\2\2\u00a6\f\3\2\2\2\u00a7\u00a8\t\5\2\2\u00a8")
        buf.write("\u00a9\t\6\2\2\u00a9\u00aa\t\7\2\2\u00aa\16\3\2\2\2\u00ab")
        buf.write("\u00ac\t\7\2\2\u00ac\u00ad\t\3\2\2\u00ad\u00ae\t\6\2\2")
        buf.write("\u00ae\u00af\t\b\2\2\u00af\20\3\2\2\2\u00b0\u00b1\t\t")
        buf.write("\2\2\u00b1\u00b2\t\n\2\2\u00b2\u00b3\t\n\2\2\u00b3\u00b4")
        buf.write("\t\b\2\2\u00b4\u00b5\t\3\2\2\u00b5\u00b6\t\6\2\2\u00b6")
        buf.write("\u00b7\t\13\2\2\u00b7\22\3\2\2\2\u00b8\u00b9\t\f\2\2\u00b9")
        buf.write("\u00ba\t\13\2\2\u00ba\u00bb\t\r\2\2\u00bb\u00bc\t\3\2")
        buf.write("\2\u00bc\u00bd\t\16\2\2\u00bd\u00be\t\3\2\2\u00be\u00bf")
        buf.write("\t\7\2\2\u00bf\24\3\2\2\2\u00c0\u00c1\t\17\2\2\u00c1\u00c2")
        buf.write("\t\r\2\2\u00c2\u00c3\t\7\2\2\u00c3\u00c4\t\f\2\2\u00c4")
        buf.write("\u00c5\t\13\2\2\u00c5\u00c6\t\16\2\2\u00c6\26\3\2\2\2")
        buf.write("\u00c7\u00c8\t\6\2\2\u00c8\u00c9\t\7\2\2\u00c9\u00ca\t")
        buf.write("\7\2\2\u00ca\u00cb\t\6\2\2\u00cb\u00cc\t\20\2\2\u00cc")
        buf.write("\30\3\2\2\2\u00cd\u00ce\t\t\2\2\u00ce\u00cf\t\7\2\2\u00cf")
        buf.write("\u00d0\t\3\2\2\u00d0\u00d1\t\6\2\2\u00d1\u00d2\t\21\2")
        buf.write("\2\u00d2\32\3\2\2\2\u00d3\u00d4\t\22\2\2\u00d4\u00d5\t")
        buf.write("\n\2\2\u00d5\u00d6\t\13\2\2\u00d6\u00d7\t\r\2\2\u00d7")
        buf.write("\u00d8\t\f\2\2\u00d8\u00d9\t\13\2\2\u00d9\u00da\t\23\2")
        buf.write("\2\u00da\u00db\t\3\2\2\u00db\34\3\2\2\2\u00dc\u00dd\t")
        buf.write("\24\2\2\u00dd\u00de\t\n\2\2\u00de\u00df\t\7\2\2\u00df")
        buf.write("\36\3\2\2\2\u00e0\u00e1\t\r\2\2\u00e1\u00e2\t\n\2\2\u00e2")
        buf.write(" \3\2\2\2\u00e3\u00e4\t\25\2\2\u00e4\u00e5\t\n\2\2\u00e5")
        buf.write("\u00e6\t\26\2\2\u00e6\u00e7\t\13\2\2\u00e7\u00e8\t\r\2")
        buf.write("\2\u00e8\u00e9\t\n\2\2\u00e9\"\3\2\2\2\u00ea\u00eb\t\25")
        buf.write("\2\2\u00eb\u00ec\t\n\2\2\u00ec$\3\2\2\2\u00ed\u00ee\t")
        buf.write("\f\2\2\u00ee\u00ef\t\24\2\2\u00ef&\3\2\2\2\u00f0\u00f1")
        buf.write("\t\r\2\2\u00f1\u00f2\t\27\2\2\u00f2\u00f3\t\3\2\2\u00f3")
        buf.write("\u00f4\t\13\2\2\u00f4(\3\2\2\2\u00f5\u00f6\t\3\2\2\u00f6")
        buf.write("\u00f7\t\b\2\2\u00f7\u00f8\t\17\2\2\u00f8\u00f9\t\3\2")
        buf.write("\2\u00f9*\3\2\2\2\u00fa\u00fb\t\7\2\2\u00fb\u00fc\t\3")
        buf.write("\2\2\u00fc\u00fd\t\r\2\2\u00fd\u00fe\t\23\2\2\u00fe\u00ff")
        buf.write("\t\7\2\2\u00ff\u0100\t\13\2\2\u0100,\3\2\2\2\u0101\u0102")
        buf.write("\t\26\2\2\u0102\u0103\t\27\2\2\u0103\u0104\t\f\2\2\u0104")
        buf.write("\u0105\t\b\2\2\u0105\u0106\t\3\2\2\u0106.\3\2\2\2\u0107")
        buf.write("\u0108\t\t\2\2\u0108\u0109\t\3\2\2\u0109\u010a\t\16\2")
        buf.write("\2\u010a\u010b\t\f\2\2\u010b\u010c\t\13\2\2\u010c\60\3")
        buf.write("\2\2\2\u010d\u010e\t\3\2\2\u010e\u010f\t\13\2\2\u010f")
        buf.write("\u0110\t\25\2\2\u0110\62\3\2\2\2\u0111\u0112\t\24\2\2")
        buf.write("\u0112\u0113\t\23\2\2\u0113\u0114\t\13\2\2\u0114\u0115")
        buf.write("\t\22\2\2\u0115\u0116\t\r\2\2\u0116\u0117\t\f\2\2\u0117")
        buf.write("\u0118\t\n\2\2\u0118\u0119\t\13\2\2\u0119\64\3\2\2\2\u011a")
        buf.write("\u011b\t\30\2\2\u011b\u011c\t\7\2\2\u011c\u011d\t\n\2")
        buf.write("\2\u011d\u011e\t\22\2\2\u011e\u011f\t\3\2\2\u011f\u0120")
        buf.write("\t\25\2\2\u0120\u0121\t\23\2\2\u0121\u0122\t\7\2\2\u0122")
        buf.write("\u0123\t\3\2\2\u0123\66\3\2\2\2\u0124\u0125\t\n\2\2\u0125")
        buf.write("\u0126\t\24\2\2\u01268\3\2\2\2\u0127\u0128\t\13\2\2\u0128")
        buf.write("\u0129\t\n\2\2\u0129\u012a\t\r\2\2\u012a:\3\2\2\2\u012b")
        buf.write("\u012c\t\26\2\2\u012c\u012d\t\f\2\2\u012d\u012e\t\r\2")
        buf.write("\2\u012e\u012f\t\27\2\2\u012f<\3\2\2\2\u0130\u0131\t\r")
        buf.write("\2\2\u0131\u0132\t\7\2\2\u0132\u0133\t\23\2\2\u0133\u0134")
        buf.write("\t\3\2\2\u0134>\3\2\2\2\u0135\u0136\t\24\2\2\u0136\u0137")
        buf.write("\t\6\2\2\u0137\u0138\t\b\2\2\u0138\u0139\t\17\2\2\u0139")
        buf.write("\u013a\t\3\2\2\u013a@\3\2\2\2\u013b\u013c\7-\2\2\u013c")
        buf.write("B\3\2\2\2\u013d\u013e\7/\2\2\u013eD\3\2\2\2\u013f\u0140")
        buf.write("\7,\2\2\u0140F\3\2\2\2\u0141\u0142\7\61\2\2\u0142H\3\2")
        buf.write("\2\2\u0143\u0144\t\25\2\2\u0144\u0145\t\f\2\2\u0145\u0146")
        buf.write("\t\5\2\2\u0146J\3\2\2\2\u0147\u0148\t\31\2\2\u0148\u0149")
        buf.write("\t\n\2\2\u0149\u014a\t\25\2\2\u014aL\3\2\2\2\u014b\u014c")
        buf.write("\t\6\2\2\u014c\u014d\t\13\2\2\u014d\u014e\t\25\2\2\u014e")
        buf.write("N\3\2\2\2\u014f\u0150\t\n\2\2\u0150\u0151\t\7\2\2\u0151")
        buf.write("P\3\2\2\2\u0152\u0153\7>\2\2\u0153\u0154\7@\2\2\u0154")
        buf.write("R\3\2\2\2\u0155\u0156\7>\2\2\u0156T\3\2\2\2\u0157\u0158")
        buf.write("\7>\2\2\u0158\u0159\7?\2\2\u0159V\3\2\2\2\u015a\u015b")
        buf.write("\7@\2\2\u015bX\3\2\2\2\u015c\u015d\7@\2\2\u015d\u015e")
        buf.write("\7?\2\2\u015eZ\3\2\2\2\u015f\u0160\7?\2\2\u0160\\\3\2")
        buf.write("\2\2\u0161\u0162\7<\2\2\u0162\u0163\7?\2\2\u0163^\3\2")
        buf.write("\2\2\u0164\u0165\7]\2\2\u0165`\3\2\2\2\u0166\u0167\7_")
        buf.write("\2\2\u0167b\3\2\2\2\u0168\u0169\7<\2\2\u0169d\3\2\2\2")
        buf.write("\u016a\u016b\7*\2\2\u016bf\3\2\2\2\u016c\u016d\7+\2\2")
        buf.write("\u016dh\3\2\2\2\u016e\u016f\7=\2\2\u016fj\3\2\2\2\u0170")
        buf.write("\u0171\7.\2\2\u0171l\3\2\2\2\u0172\u0176\t\32\2\2\u0173")
        buf.write("\u0175\t\33\2\2\u0174\u0173\3\2\2\2\u0175\u0178\3\2\2")
        buf.write("\2\u0176\u0174\3\2\2\2\u0176\u0177\3\2\2\2\u0177n\3\2")
        buf.write("\2\2\u0178\u0176\3\2\2\2\u0179\u017b\t\34\2\2\u017a\u0179")
        buf.write("\3\2\2\2\u017b\u017c\3\2\2\2\u017c\u017a\3\2\2\2\u017c")
        buf.write("\u017d\3\2\2\2\u017d\u017e\3\2\2\2\u017e\u017f\b8\2\2")
        buf.write("\u017fp\3\2\2\2\u0180\u0181\7*\2\2\u0181\u0182\7,\2\2")
        buf.write("\u0182\u0186\3\2\2\2\u0183\u0185\13\2\2\2\u0184\u0183")
        buf.write("\3\2\2\2\u0185\u0188\3\2\2\2\u0186\u0187\3\2\2\2\u0186")
        buf.write("\u0184\3\2\2\2\u0187\u0189\3\2\2\2\u0188\u0186\3\2\2\2")
        buf.write("\u0189\u018a\7,\2\2\u018a\u018b\7+\2\2\u018b\u018c\3\2")
        buf.write("\2\2\u018c\u018d\b9\2\2\u018dr\3\2\2\2\u018e\u0192\7}")
        buf.write("\2\2\u018f\u0191\13\2\2\2\u0190\u018f\3\2\2\2\u0191\u0194")
        buf.write("\3\2\2\2\u0192\u0193\3\2\2\2\u0192\u0190\3\2\2\2\u0193")
        buf.write("\u0195\3\2\2\2\u0194\u0192\3\2\2\2\u0195\u0196\7\177\2")
        buf.write("\2\u0196\u0197\3\2\2\2\u0197\u0198\b:\2\2\u0198t\3\2\2")
        buf.write("\2\u0199\u019a\7\61\2\2\u019a\u019b\7\61\2\2\u019b\u019f")
        buf.write("\3\2\2\2\u019c\u019e\n\35\2\2\u019d\u019c\3\2\2\2\u019e")
        buf.write("\u01a1\3\2\2\2\u019f\u019d\3\2\2\2\u019f\u01a0\3\2\2\2")
        buf.write("\u01a0\u01a2\3\2\2\2\u01a1\u019f\3\2\2\2\u01a2\u01a3\b")
        buf.write(";\2\2\u01a3v\3\2\2\2\u01a4\u01a5\7*\2\2\u01a5\u01a6\7")
        buf.write(",\2\2\u01a6\u01aa\3\2\2\2\u01a7\u01a9\13\2\2\2\u01a8\u01a7")
        buf.write("\3\2\2\2\u01a9\u01ac\3\2\2\2\u01aa\u01ab\3\2\2\2\u01aa")
        buf.write("\u01a8\3\2\2\2\u01ab\u01ad\3\2\2\2\u01ac\u01aa\3\2\2\2")
        buf.write("\u01ad\u01ae\7,\2\2\u01ae\u01af\7+\2\2\u01af\u01b0\3\2")
        buf.write("\2\2\u01b0\u01b1\b<\2\2\u01b1x\3\2\2\2\u01b2\u01b6\7}")
        buf.write("\2\2\u01b3\u01b5\13\2\2\2\u01b4\u01b3\3\2\2\2\u01b5\u01b8")
        buf.write("\3\2\2\2\u01b6\u01b7\3\2\2\2\u01b6\u01b4\3\2\2\2\u01b7")
        buf.write("\u01b9\3\2\2\2\u01b8\u01b6\3\2\2\2\u01b9\u01ba\7\177\2")
        buf.write("\2\u01ba\u01bb\3\2\2\2\u01bb\u01bc\b=\2\2\u01bcz\3\2\2")
        buf.write("\2\u01bd\u01be\7\61\2\2\u01be\u01bf\7\61\2\2\u01bf\u01c3")
        buf.write("\3\2\2\2\u01c0\u01c2\n\35\2\2\u01c1\u01c0\3\2\2\2\u01c2")
        buf.write("\u01c5\3\2\2\2\u01c3\u01c1\3\2\2\2\u01c3\u01c4\3\2\2\2")
        buf.write("\u01c4\u01c6\3\2\2\2\u01c5\u01c3\3\2\2\2\u01c6\u01c7\b")
        buf.write(">\2\2\u01c7|\3\2\2\2\u01c8\u01c9\7^\2\2\u01c9\u01ca\t")
        buf.write("\36\2\2\u01ca~\3\2\2\2\u01cb\u01d0\7$\2\2\u01cc\u01cf")
        buf.write("\5}?\2\u01cd\u01cf\n\37\2\2\u01ce\u01cc\3\2\2\2\u01ce")
        buf.write("\u01cd\3\2\2\2\u01cf\u01d2\3\2\2\2\u01d0\u01ce\3\2\2\2")
        buf.write("\u01d0\u01d1\3\2\2\2\u01d1\u01d3\3\2\2\2\u01d2\u01d0\3")
        buf.write("\2\2\2\u01d3\u01d4\7$\2\2\u01d4\u01d5\b@\3\2\u01d5\u0080")
        buf.write("\3\2\2\2\u01d6\u01db\7$\2\2\u01d7\u01da\5}?\2\u01d8\u01da")
        buf.write("\n \2\2\u01d9\u01d7\3\2\2\2\u01d9\u01d8\3\2\2\2\u01da")
        buf.write("\u01dd\3\2\2\2\u01db\u01d9\3\2\2\2\u01db\u01dc\3\2\2\2")
        buf.write("\u01dc\u01df\3\2\2\2\u01dd\u01db\3\2\2\2\u01de\u01e0\t")
        buf.write("!\2\2\u01df\u01de\3\2\2\2\u01e0\u01e1\3\2\2\2\u01e1\u01e2")
        buf.write("\bA\4\2\u01e2\u0082\3\2\2\2\u01e3\u01e8\7$\2\2\u01e4\u01e7")
        buf.write("\5}?\2\u01e5\u01e7\n\"\2\2\u01e6\u01e4\3\2\2\2\u01e6\u01e5")
        buf.write("\3\2\2\2\u01e7\u01ea\3\2\2\2\u01e8\u01e9\3\2\2\2\u01e8")
        buf.write("\u01e6\3\2\2\2\u01e9\u01eb\3\2\2\2\u01ea\u01e8\3\2\2\2")
        buf.write("\u01eb\u01ec\t#\2\2\u01ec\u01ed\n\36\2\2\u01ed\u01ee\3")
        buf.write("\2\2\2\u01ee\u01f3\bB\5\2\u01ef\u01f2\5}?\2\u01f0\u01f2")
        buf.write("\n\35\2\2\u01f1\u01ef\3\2\2\2\u01f1\u01f0\3\2\2\2\u01f2")
        buf.write("\u01f5\3\2\2\2\u01f3\u01f1\3\2\2\2\u01f3\u01f4\3\2\2\2")
        buf.write("\u01f4\u01f6\3\2\2\2\u01f5\u01f3\3\2\2\2\u01f6\u01f7\7")
        buf.write("$\2\2\u01f7\u0084\3\2\2\2\u01f8\u01f9\13\2\2\2\u01f9\u01fa")
        buf.write("\bC\6\2\u01fa\u0086\3\2\2\2\31\2\u008a\u008f\u0092\u009e")
        buf.write("\u00a2\u0176\u017c\u0186\u0192\u019f\u01aa\u01b6\u01c3")
        buf.write("\u01ce\u01d0\u01d9\u01db\u01df\u01e6\u01e8\u01f1\u01f3")
        buf.write("\7\b\2\2\3@\2\3A\3\3B\4\3C\5")
        return buf.getvalue()


class MPLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    INTLIT = 1
    REALLIT = 2
    BOOLLIT = 3
    DOUBLE_DOT = 4
    VAR = 5
    REAL = 6
    BOOLEAN = 7
    INTEGER = 8
    STRING = 9
    ARRAY = 10
    BREAK = 11
    CONTINUE = 12
    FOR = 13
    TO = 14
    DOWNTO = 15
    DO = 16
    IF = 17
    THEN = 18
    ELSE = 19
    RETURN = 20
    WHILE = 21
    BEGIN = 22
    END = 23
    FUNCTION = 24
    PROCEDURE = 25
    OF = 26
    NOTOP = 27
    WITH = 28
    TRUE = 29
    FALSE = 30
    ADDOP = 31
    SUBOP = 32
    MULOP = 33
    DIVOP = 34
    INT_DIVOP = 35
    MODOP = 36
    AND = 37
    OR = 38
    NEOP = 39
    LTOP = 40
    LEOP = 41
    GTOP = 42
    GEOP = 43
    EQOP = 44
    ASSIGNMENT = 45
    LSB = 46
    RSB = 47
    COLON = 48
    LB = 49
    RB = 50
    SEMI = 51
    COMA = 52
    ID = 53
    WS = 54
    TRAINDITIONNAL_COMMENT = 55
    BLOCK_COMMENT = 56
    LINE_COMMENT = 57
    TRADITIONAL_BLOCK_CMT = 58
    BLOCK_CMT = 59
    LINE_CMT = 60
    STRINGLIT = 61
    UNCLOSE_STRING = 62
    ILLEGAL_ESCAPE = 63
    ERROR_CHAR = 64

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'..'", "'+'", "'-'", "'*'", "'/'", "'<>'", "'<'", "'<='", "'>'", 
            "'>='", "'='", "':='", "'['", "']'", "':'", "'('", "')'", "';'", 
            "','" ]

    symbolicNames = [ "<INVALID>",
            "INTLIT", "REALLIT", "BOOLLIT", "DOUBLE_DOT", "VAR", "REAL", 
            "BOOLEAN", "INTEGER", "STRING", "ARRAY", "BREAK", "CONTINUE", 
            "FOR", "TO", "DOWNTO", "DO", "IF", "THEN", "ELSE", "RETURN", 
            "WHILE", "BEGIN", "END", "FUNCTION", "PROCEDURE", "OF", "NOTOP", 
            "WITH", "TRUE", "FALSE", "ADDOP", "SUBOP", "MULOP", "DIVOP", 
            "INT_DIVOP", "MODOP", "AND", "OR", "NEOP", "LTOP", "LEOP", "GTOP", 
            "GEOP", "EQOP", "ASSIGNMENT", "LSB", "RSB", "COLON", "LB", "RB", 
            "SEMI", "COMA", "ID", "WS", "TRAINDITIONNAL_COMMENT", "BLOCK_COMMENT", 
            "LINE_COMMENT", "TRADITIONAL_BLOCK_CMT", "BLOCK_CMT", "LINE_CMT", 
            "STRINGLIT", "UNCLOSE_STRING", "ILLEGAL_ESCAPE", "ERROR_CHAR" ]

    ruleNames = [ "INTLIT", "REALLIT", "REAL_NUMBER", "BOOLLIT", "DOUBLE_DOT", 
                  "VAR", "REAL", "BOOLEAN", "INTEGER", "STRING", "ARRAY", 
                  "BREAK", "CONTINUE", "FOR", "TO", "DOWNTO", "DO", "IF", 
                  "THEN", "ELSE", "RETURN", "WHILE", "BEGIN", "END", "FUNCTION", 
                  "PROCEDURE", "OF", "NOTOP", "WITH", "TRUE", "FALSE", "ADDOP", 
                  "SUBOP", "MULOP", "DIVOP", "INT_DIVOP", "MODOP", "AND", 
                  "OR", "NEOP", "LTOP", "LEOP", "GTOP", "GEOP", "EQOP", 
                  "ASSIGNMENT", "LSB", "RSB", "COLON", "LB", "RB", "SEMI", 
                  "COMA", "ID", "WS", "TRAINDITIONNAL_COMMENT", "BLOCK_COMMENT", 
                  "LINE_COMMENT", "TRADITIONAL_BLOCK_CMT", "BLOCK_CMT", 
                  "LINE_CMT", "ESCAPE", "STRINGLIT", "UNCLOSE_STRING", "ILLEGAL_ESCAPE", 
                  "ERROR_CHAR" ]

    grammarFileName = "MP.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


    def action(self, localctx:RuleContext, ruleIndex:int, actionIndex:int):
        if self._actions is None:
            actions = dict()
            actions[62] = self.STRINGLIT_action 
            actions[63] = self.UNCLOSE_STRING_action 
            actions[64] = self.ILLEGAL_ESCAPE_action 
            actions[65] = self.ERROR_CHAR_action 
            self._actions = actions
        action = self._actions.get(ruleIndex, None)
        if action is not None:
            action(localctx, actionIndex)
        else:
            raise Exception("No registered action for:" + str(ruleIndex))

    def STRINGLIT_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 0:

            				self.text = self.text[1:-1]
            			
     

    def UNCLOSE_STRING_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 1:

            				raise UncloseString(self.text[1:])
            			
     

    def ILLEGAL_ESCAPE_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 2:

            				raise IllegalEcape(self.text[1:])
            			
     

    def ERROR_CHAR_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 3:
            raise ErrorToken(self.text)
     


