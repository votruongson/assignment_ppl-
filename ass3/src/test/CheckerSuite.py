import unittest
from TestUtils import TestChecker
from AST import *
from StaticCheck import *

class CheckerSuite(unittest.TestCase):
    
    # def test_question_1_Redecl_var(self):
    #     """ More Complex progress"""
    #     input = Program ([
    #         VarDecl(Id("a"),IntType()),
    #         FuncDecl(Id('main'),[],[],[]),
    #         VarDecl(Id('a'),FloatType())
    #         ])

    #     expect = "Redeclared Variable: a"
    #     self.assertTrue(TestChecker.test(input,expect,401))

    # def test_question_2_Redecl_var_para(self):
    #     """ More Complex progress"""
    #     input = Program ([
    #         VarDecl(Id("a"),IntType()),
    #         FuncDecl(Id('main'),[VarDecl(Id("a"),IntType()),VarDecl(Id("a"),IntType())],[],[]),
    #         VarDecl(Id('b'),FloatType())
    #         ])

    #     expect = "Redeclared Parameter: a"
    #     self.assertTrue(TestChecker.test(input,expect,402))

    # def test_question_3_Redecl_global_first(self):
    #     """ More Complex progress"""
    #     input = Program ([
    #         VarDecl(Id('main'),IntType()),
    #         FuncDecl(Id('main'),[VarDecl(Id("c"),IntType()),VarDecl(Id("a"),IntType())],[],[]),
    #         VarDecl(Id('b'),FloatType())
    #         ])

    #     expect = "Redeclared Procedure: main"
    #     self.assertTrue(TestChecker.test(input,expect,403))

    # def test_question_4_Redecl_local(self):
    #     """ More Complex progress"""
    #     input = Program ([
    #         VarDecl(Id('a'),IntType()),
    #         FuncDecl(Id('main'),[],[VarDecl(Id("c"),IntType()),VarDecl(Id("c"),FloatType())],[]),
    #         VarDecl(Id('b'),FloatType())
    #         ])

    #     expect = "Redeclared Variable: c"
    #     self.assertTrue(TestChecker.test(input,expect,404))

    # def test_question_5_Redecl_global_para(self):
    #     """ More Complex progress"""
    #     input = Program ([
    #         VarDecl(Id('main'),IntType()),
    #         FuncDecl(Id('main'),[VarDecl(Id("c"),IntType()),VarDecl(Id("c"),FloatType())],[],[],IntType()),
    #         VarDecl(Id('b'),FloatType())
    #         ])

    #     expect = "Redeclared Function: main"
    #     self.assertTrue(TestChecker.test(input,expect,405))

    # def test_question_6_Redecl_global_local(self):
    #     """ More Complex progress"""
    #     input = Program ([
    #         VarDecl(Id('main'),IntType()),
    #         FuncDecl(Id('main'),[],[VarDecl(Id("c"),IntType()),VarDecl(Id("c"),FloatType())],[],IntType()),
    #         VarDecl(Id('b'),FloatType())
    #         ])

    #     expect = "Redeclared Function: main"
    #     self.assertTrue(TestChecker.test(input,expect,406))

    # def test_question_7_Redecl_para_first_local(self):
    #     """ More Complex progress"""
    #     input = Program ([
    #         VarDecl(Id('a'),IntType()),
    #         FuncDecl(Id('main'),[VarDecl(Id("a"),IntType()),VarDecl(Id("a"),FloatType())],[VarDecl(Id("c"),IntType()),VarDecl(Id("c"),FloatType())],[],IntType()),
    #         VarDecl(Id('b'),FloatType())
    #         ])

    #     expect = "Redeclared Parameter: a"
    #     self.assertTrue(TestChecker.test(input,expect,407))

    # def test_question_8_Redecl_para_local(self):
    #     """ More Complex progress"""
    #     input = Program ([
    #         VarDecl(Id('a'),IntType()),
    #         FuncDecl(Id('main'),[VarDecl(Id("a"),IntType()),VarDecl(Id("b"),FloatType())],[VarDecl(Id("b"),IntType()),VarDecl(Id("c"),FloatType())],[],IntType()),
    #         VarDecl(Id('b'),FloatType())
    #         ])

    #     expect = "Redeclared Variable: b"
    #     self.assertTrue(TestChecker.test(input,expect,408))

    # def test_undeclared_identifier1(self):
    #     input = Program ([
    #         VarDecl(Id("a"),StringType()),
    #         VarDecl(Id("c"),StringType()),
    #         VarDecl(Id("d"),IntType()),
    #         FuncDecl(Id("main"),[],[],[CallStmt(Id("putIntLn"),[Id("b")])])
    #         ])
    #     expect = "Undeclared Identifier: b"
    #     self.assertTrue(TestChecker.test(input,expect,411))



    # def test_undeclared_procedure(self):
    #     input = Program ([
    #         VarDecl(Id("a"),StringType()),
    #         VarDecl(Id("c"),StringType()),
    #         VarDecl(Id("d"),IntType()),
    #         FuncDecl(Id("main"),[],[],[CallStmt(Id("convert"),[Id("b")])])
    #         ])
    #     expect = "Undeclared Procedure: convert"
    #     self.assertTrue(TestChecker.test(input,expect,412))

    # def test_undeclared_procedure1(self):
    #     input = Program ([
    #         VarDecl(Id("a"),StringType()),
    #         VarDecl(Id("c"),StringType()),
    #         VarDecl(Id("d"),IntType()),
    #         FuncDecl(Id("sondeptrai"),[],[],[],IntType()),
    #         FuncDecl(Id("main"),[],[],[CallStmt(Id("sondeptrai"),[])])
    #         ])
    #     expect = "Undeclared Procedure: sondeptrai"
    #     self.assertTrue(TestChecker.test(input,expect,413))

    # def test_undeclared_function1(self):
    #     input = Program ([
    #         VarDecl(Id("a"),StringType()),
    #         VarDecl(Id("c"),StringType()),
    #         VarDecl(Id("d"),IntType()),
    #         FuncDecl(Id("main"),[],[],[CallExpr(Id("sondeptrai"),[])])
    #         ])
    #     expect = "Undeclared Function: sondeptrai"
    #     self.assertTrue(TestChecker.test(input,expect,414))

    # def test_undeclared_function1(self):
    #     input = Program ([
    #         VarDecl(Id("a"),StringType()),
    #         VarDecl(Id("c"),StringType()),
    #         VarDecl(Id("d"),IntType()),
    #         FuncDecl(Id("main"),[],[],[Id("b")])
    #         ])
    #     expect = "Undeclared Identifier: b"
    #     self.assertTrue(TestChecker.test(input,expect,415))

    # def test_undeclared_function1(self):
    #     input = Program ([
    #         VarDecl(Id("a"),StringType()),
    #         VarDecl(Id("c"),StringType()),
    #         VarDecl(Id("d"),IntType()),
    #         FuncDecl(Id("main"),[],[],[If(BooleanLiteral(True),[Id("b")])])
    #         ])
    #     expect = "Undeclared Identifier: b"
    #     self.assertTrue(TestChecker.test(input,expect,416))

    # def test_type_mismatch_stmt1(self):
    #     input = Program ([
            
    #         FuncDecl(Id("main"),[],[],[CallStmt(Id("putIntLn"),[UnaryOp("-",FloatLiteral(1))])])
    #         ])
    #     expect = "Type Mismatch In Statement: CallStmt(Id(putIntLn),[UnaryOp(-,FloatLiteral(1))])"
    #     self.assertTrue(TestChecker.test(input,expect,421))

    # def test_type_mismatch_stmt2(self):
    #     input = Program ([
            
    #         FuncDecl(Id("main"),[],[],[CallStmt(Id("putIntLn"),[UnaryOp("not",BooleanLiteral(True))])])
    #         ])
    #     expect = "Type Mismatch In Statement: CallStmt(Id(putIntLn),[UnaryOp(not,BooleanLiteral(True))])"
    #     self.assertTrue(TestChecker.test(input,expect,422))

    # def test_type_mismatch_stmt3(self):
    #     input = Program ([
            
    #         FuncDecl(Id("main"),[],[],[CallStmt(Id("putIntLn"),[BinaryOp("+",FloatLiteral(1),IntLiteral(2))])])
    #         ])
    #     expect = "Type Mismatch In Statement: CallStmt(Id(putIntLn),[BinaryOp(+,FloatLiteral(1),IntLiteral(2))])"
    #     self.assertTrue(TestChecker.test(input,expect,423))

    # def test_type_mismatch_stmt4(self):
    #     input = Program ([
    #         FuncDecl(Id("main"),[],[],[Return(IntLiteral(1))],FloatType())
    #         ])
    #     expect = "Type Mismatch In Statement: Return(Some(IntLiteral(1)))"
    #     self.assertTrue(TestChecker.test(input,expect,424))

    # def test_30_undecl_proce(self):
    #     input = Program ([
    #         FuncDecl(Id("main"),[],[],[If(BooleanLiteral("true"),[CallStmt(Id("foo"),[])])])
    #         ])
    #     expect = "Undeclared Procedure: foo"
    #     self.assertTrue(TestChecker.test(input,expect,430))



    

    # def test_type_mismatch_exp_unary(self):
    #     input = Program ([
    #         VarDecl(Id("a"),StringType()),
    #         VarDecl(Id("c"),StringType()),
    #         VarDecl(Id("d"),IntType()),
    #         FuncDecl(Id("main"),[],[],[CallStmt(Id("putIntLn"),[UnaryOp("not",IntLiteral(1))])])
    #         ])
    #     expect = "Type Mismatch In Expression: UnaryOp(not,IntLiteral(1))"
    #     self.assertTrue(TestChecker.test(input,expect,441))

    # def test_type_mismatch_exp_unary2(self):
    #     input = Program ([
    #         VarDecl(Id("a"),StringType()),
    #         VarDecl(Id("c"),StringType()),
    #         VarDecl(Id("d"),IntType()),
    #         FuncDecl(Id("main"),[],[],[CallStmt(Id("putIntLn"),[UnaryOp("not",FloatLiteral(1))])])
    #         ])
    #     expect = "Type Mismatch In Expression: UnaryOp(not,FloatLiteral(1))"
    #     self.assertTrue(TestChecker.test(input,expect,442))

    # def test_type_mismatch_exp_unary3(self):
    #     input = Program ([
    #         VarDecl(Id("a"),StringType()),
    #         VarDecl(Id("c"),StringType()),
    #         VarDecl(Id("d"),IntType()),
    #         FuncDecl(Id("main"),[],[],[CallStmt(Id("putIntLn"),[UnaryOp("not",StringLiteral("truongson"))])])
    #         ])
    #     expect = "Type Mismatch In Expression: UnaryOp(not,StringLiteral(truongson))"
    #     self.assertTrue(TestChecker.test(input,expect,443))

    # def test_type_mismatch_exp_unary4(self):
    #     input = Program ([
    #         VarDecl(Id("a"),StringType()),
    #         VarDecl(Id("c"),StringType()),
    #         VarDecl(Id("d"),IntType()),
    #         FuncDecl(Id("main"),[],[],[CallStmt(Id("putIntLn"),[UnaryOp("-",StringLiteral("truongson"))])])
    #         ])
    #     expect = "Type Mismatch In Expression: UnaryOp(-,StringLiteral(truongson))"
    #     self.assertTrue(TestChecker.test(input,expect,444))

    # def test_type_mismatch_exp_unary4(self):
    #     input = Program ([
    #         VarDecl(Id("a"),StringType()),
    #         VarDecl(Id("c"),StringType()),
    #         VarDecl(Id("d"),IntType()),
    #         FuncDecl(Id("main"),[],[],[CallStmt(Id("putIntLn"),[UnaryOp("-",BooleanLiteral(True))])])
    #         ])
    #     expect = "Type Mismatch In Expression: UnaryOp(-,BooleanLiteral(True))"
    #     self.assertTrue(TestChecker.test(input,expect,445))

    # def test_type_mismatch_exp_binary(self):
    #     input = Program ([
           
    #         FuncDecl(Id("main"),[],[],[CallStmt(Id("putIntLn"),[BinaryOp("-",BooleanLiteral(True),IntLiteral(1))])])
    #         ])
    #     expect = "Type Mismatch In Expression: BinaryOp(-,BooleanLiteral(True),IntLiteral(1))"
    #     self.assertTrue(TestChecker.test(input,expect,446))

    # def test_type_mismatch_exp_binary2(self):
    #     input = Program ([
           
    #         FuncDecl(Id("main"),[],[],[CallStmt(Id("putIntLn"),[BinaryOp("=",BooleanLiteral(True),IntLiteral(1))])])
    #         ])
    #     expect = "Type Mismatch In Expression: BinaryOp(=,BooleanLiteral(True),IntLiteral(1))"
    #     self.assertTrue(TestChecker.test(input,expect,447))

    # def test_type_mismatch_exp_binary3(self):
    #     input = Program ([
           
    #         FuncDecl(Id("main"),[],[],[CallStmt(Id("putIntLn"),[BinaryOp("/",BooleanLiteral(True),IntLiteral(1))])])
    #         ])
    #     expect = "Type Mismatch In Expression: BinaryOp(/,BooleanLiteral(True),IntLiteral(1))"
    #     self.assertTrue(TestChecker.test(input,expect,448))

    # def test_type_mismatch_exp_binary4(self):
    #     input = Program ([
           
    #         FuncDecl(Id("main"),[],[],[CallStmt(Id("putIntLn"),[BinaryOp("div",BooleanLiteral(True),IntLiteral(1))])])
    #         ])
    #     expect = "Type Mismatch In Expression: BinaryOp(div,BooleanLiteral(True),IntLiteral(1))"
    #     self.assertTrue(TestChecker.test(input,expect,449))

    # def test_type_mismatch_exp_binary4(self):
    #     input = Program ([
           
    #         FuncDecl(Id("main"),[],[],[CallStmt(Id("putIntLn"),[BinaryOp("and",BooleanLiteral(True),IntLiteral(1))])])
    #         ])
    #     expect = "Type Mismatch In Expression: BinaryOp(and,BooleanLiteral(True),IntLiteral(1))"
    #     self.assertTrue(TestChecker.test(input,expect,450))

    # def test_type_mismatch_exp_binary4(self):
    #     input = Program ([
           
    #         FuncDecl(Id("main"),[],[],[CallStmt(Id("putIntLn"),[BinaryOp("andthen",BooleanLiteral(True),IntLiteral(1))])])
    #         ])
    #     expect = "Type Mismatch In Expression: BinaryOp(andthen,BooleanLiteral(True),IntLiteral(1))"
    #     self.assertTrue(TestChecker.test(input,expect,451))

    # def test_undecl_proc1(self):
    #     input = Program([
    #         FuncDecl(Id("foo"),[],[],[CallStmt(Id("foo1"),[])])
    #     ])
    #     expect = 'Undeclared Procedure: foo1'
    #     self.assertTrue(TestChecker.test(input,expect,417))

    # def test_undecl_func1(self):
    #     input = Program([
    #         FuncDecl(Id("foo"),[],[],[CallExpr(Id("foo1"),[])])
    #     ])
    #     expect = 'Undeclared Function: foo1'
    #     self.assertTrue(TestChecker.test(input,expect,418))

    # def test_undecl_func2(self):
    #     input = Program([
    #         FuncDecl(Id("foo"),[VarDecl(Id("n"),IntType())],[],[CallStmt(Id("foo"),[CallExpr(Id("foo1"),[])])])
    #     ])
    #     expect = 'Undeclared Function: foo1'
    #     self.assertTrue(TestChecker.test(input,expect,419))

    # def test_undecl_id1(self):
    #     input = Program([
    #         FuncDecl(Id("foo"),[VarDecl(Id("n"),IntType())],[],[CallStmt(Id("foo"),[Id("m")])])
    #     ])
    #     expect = 'Undeclared Identifier: m'
    #     self.assertTrue(TestChecker.test(input,expect,4200))

    # def test_undecl_id1(self):
    #     input = Program([
    #         FuncDecl(Id('func'),[VarDecl(Id('b'),IntType())],[],
    #             [Assign(Id("b"),CallExpr(Id("foo"),[IntLiteral(3)])),Return(IntLiteral(3))],IntType()),
    #         FuncDecl(Id('foo'),[VarDecl(Id('b'),IntType())],[],[Return(IntLiteral(3))],IntType()),
    #         FuncDecl(Id('main'),[],[],[])

    #     ])
    #     expect = 'Undeclared Function: foo'
    #     self.assertTrue(TestChecker.test(input,expect,4201))

    # def test_undecl_id1(self):
    #     input = Program([
    #         VarDecl(Id('a'),IntType()),
    #         FuncDecl(Id('funcA'),[],[VarDecl(Id('b'),IntType())],
    #             [Assign(Id("a"),IntLiteral(7)),
    #             Assign(Id("b"),Id("a"))]),
    #         FuncDecl(Id('sum'),
    #             [VarDecl(Id('b'),IntType())],
    #             [VarDecl(Id("d"),IntType())],
    #             [Assign(Id("d"),IntLiteral(7)),
    #                 Return(BinaryOp("+",Id("a"),BinaryOp("+",Id("b"),Id("d"))))],
    #             IntType()),
    #         FuncDecl(Id('main'),
    #             [],
    #             [VarDecl(Id("m"),ArrayType(IntLiteral(1),IntLiteral(10),IntType()))],
    #             [Assign(ArrayCell(Id("m"),IntLiteral(1)),CallExpr(Id("sum"),[IntLiteral(3)])),
    #                 CallStmt(Id("funcA"),[]),
    #                 Assign(Id("a"),BinaryOp("+",IntLiteral(1),ArrayCell(Id("n"),IntLiteral(1)))),
    #                 Return()
    #                 ])

    #     ])
    #     expect = 'Undeclared Identifier: n'
    #     self.assertTrue(TestChecker.test(input,expect,4202))

    # def test_tymistch_in_stmt10(self):
    #     input = Program([
    #         VarDecl(Id('a'),IntType()),
    #         VarDecl(Id('b'),FloatType()),
    #         VarDecl(Id('m'),ArrayType(IntLiteral(1),IntLiteral(10),IntType())),
    #         FuncDecl(Id('main'),
    #             [],
    #             [],
    #             [Assign(Id("b"),BinaryOp("+",ArrayCell(Id("m"),IntLiteral(1)),UnaryOp("-",IntLiteral(1)))),
    #                 Assign(Id("b"),BinaryOp("*",Id("b"),BinaryOp("+",FloatLiteral(1.0),FloatLiteral(1)))),
    #                 Assign(Id("b"),UnaryOp("not",BinaryOp("=",ArrayCell(Id("m"),IntLiteral(1)),IntLiteral(1)))),
    #                 Return()
    #                 ])

    #     ])
    #     expect = "Type Mismatch In Statement: AssignStmt(Id(b),UnaryOp(not,BinaryOp(=,ArrayCell(Id(m),IntLiteral(1)),IntLiteral(1))))"
    #     self.assertTrue(TestChecker.test(input,expect,4203))

    def test_tymistch_in_stmt11(self):
        input = Program([
    VarDecl(Id('a'),IntType()),
    FuncDecl(Id('foo'),
        [VarDecl(Id('a'),IntType()),VarDecl(Id('b'),IntType())],
        [],
        [If(BinaryOp('>',Id('a'),Id('b')),
            [Assign(Id('a'),BinaryOp('+',IntLiteral(1),Id('b')))],
            [Assign(Id('a'),BinaryOp('+',Id('b'),IntLiteral(2)))]),
        Return(Id('a'))],IntType()),
    FuncDecl(Id('foo1'),
        [VarDecl(Id('a'),IntType())],
        [VarDecl(Id('b'),IntType()),VarDecl(Id('c'),IntType()),VarDecl(Id('d'),IntType())],
        [Assign(Id('b'),IntLiteral(2)),
        Assign(Id('c'),IntLiteral(3)),
        If(BinaryOp('>',Id('a'),Id('b')),
            [Assign(Id('d'),BinaryOp('+',Id('a'),Id('c')))],
            [Assign(Id('d'),BinaryOp('+',Id('b'),CallExpr(Id('foo2'),[IntLiteral(1)])))]),
        Return(Id('d'))],IntType()),
    VarDecl(Id('b'),IntType()),
    FuncDecl(Id('foo2'),
        [VarDecl(Id('a'),IntType())],
        [],
        [While(BinaryOp('>',Id('a'),IntLiteral(5)),[Assign(Id('a'),BinaryOp('+',Id('a'),IntLiteral(1)))]),
        Return(Id('a'))],IntType()),
    FuncDecl(Id('main'),
        [],
        [],
        [Assign(Id('a'),CallExpr(Id('foo'),[CallExpr(Id('foo1'),[IntLiteral(1)]),CallExpr(Id('foo2'),[IntLiteral(2)])])),
        CallStmt(Id('funy'),[IntLiteral(4)]),Return(None)])
    ])
        expect = "Undeclared Procedure: funy"
        self.assertTrue(TestChecker.test(input,expect,4204))

    


























    






#     def test_typeMistack(self):
#         input = Program([
#             FuncDecl(Id("main"),[],[],[
#                 CallStmt(Id("putIntLn"),[BinaryOp('+',IntLiteral(1),IntLiteral(4))]),
#                 CallStmt(Id("putIntLn"),[BinaryOp('/',IntLiteral(1),IntLiteral(4))])])
#             ])
#         expect = "Type Mismatch In Statement: CallStmt(Id(putIntLn),[BinaryOp(/,IntLiteral(1),IntLiteral(4))])"
#         self.assertTrue(TestChecker.test(input,expect,453))


#     def test_redeclare_procedure(self):
#         input = Program([
#                     VarDecl(Id("a"),StringType()),
#                     FuncDecl(Id("main"),[],[],[]),
#                     FuncDecl(Id("main"),[VarDecl(Id("b"),IntType())],[],[])
#                     ])
#         expect = "Redeclared Procedure: main"
#         self.assertTrue(TestChecker.test(input,expect,454))

#     def test_no_entry_point_1(self):
#         input = Program([
#                     VarDecl(Id("a"),StringType()),
#                     FuncDecl(Id("b"),[],[],[]),
#                     FuncDecl(Id("c"),[],[],[])
#                     ])
#         expect = "No entry point"
#         self.assertTrue(TestChecker.test(input,expect,455))
    
#     def test_no_entry_point_2(self):
#         input = Program([
#                     VarDecl(Id("a"),StringType()),
#                     FuncDecl(Id("main"),[],[],[], IntType),
#                     FuncDecl(Id("c"),[],[],[])
#                     ])
#         expect = "No entry point"
#         self.assertTrue(TestChecker.test(input,expect,456))

#     def test_no_entry_point_x(self):
#         input = Program([
#                     VarDecl(Id("a"),StringType()),
#                     FuncDecl(Id("main"),[],[VarDecl(Id("b"),IntType()),VarDecl(Id("c"),BoolType())],[])
#                     ])
#         expect = "[Symbol(main,MType([],VoidType())),Symbol(a,StringType)]"
#         self.assertTrue(TestChecker.test(input,expect,457))
    
#     def test_undeclared_identifier1(self):
#         input = Program([
#                     VarDecl(Id("a"),StringType()),
#                     FuncDecl(Id("main"),[],[],[CallStmt(Id("putIntLn"),[Id("b")])])
#                     ])
#         expect = "Undeclared Identifier: b"
#         self.assertTrue(TestChecker.test(input,expect,458))

# ### nho fix type undeclare in callstatement
#     def test_type_mismatch_in_if(self):
#         input = Program([
#                     VarDecl(Id("a"),StringType()),
#                     FuncDecl(Id("main"),[],[], 
#                         [If(Id("a"),[],[]) ])
#                     ])
#         expect = "Type Mismatch In Statement: If(Id(a),[],[])"
#         self.assertTrue(TestChecker.test(input,expect,459))

#     def test_type_mismatch_in_for(self):
#         input = Program([
#                     VarDecl(Id("a"),StringType()),
#                     FuncDecl(Id("main"),[],[], 
#                         [For(Id("a"), IntLiteral(1), IntLiteral(5), True, []) ])
#                     ])
#         expect = "Type Mismatch In Statement: For(Id(a)IntLiteral(1),IntLiteral(5),True,[])"
#         self.assertTrue(TestChecker.test(input,expect,460))

#     def test_type_mismatch_in_for_2(self):
#         input = Program([
#                     VarDecl(Id("a"),IntType()),
#                     VarDecl(Id("b"),FloatType()),
#                     VarDecl(Id("c"),IntType()),
#                     FuncDecl(Id("main"),[],[], 
#                         [For(Id("a"), Id("b"), Id("c"), True, []) ])
#                     ])
#         expect = "Type Mismatch In Statement: For(Id(a)Id(b),Id(c),True,[])"
#         self.assertTrue(TestChecker.test(input,expect,461))

#     def test_type_mismatch_in_for_3(self):
#         input = Program([
#                     VarDecl(Id("a"),IntType()),
#                     VarDecl(Id("b"),IntType()),
#                     VarDecl(Id("c"),FloatType()),
#                     FuncDecl(Id("main"),[],[], 
#                         [For(Id("a"), Id("b"), Id("c"), True, []) ])
#                     ])
#         expect = "Type Mismatch In Statement: For(Id(a)Id(b),Id(c),True,[])"
#         self.assertTrue(TestChecker.test(input,expect,462))

#     def test_type_mismatch_in_while_1(self):
#         input = Program([
#                     VarDecl(Id("a"),IntType()),
#                     FuncDecl(Id("main"),[],[], 
#                         [ While(Id("a"),[]) ])
#                     ])
#         expect = "Type Mismatch In Statement: While(Id(a),[])"
#         self.assertTrue(TestChecker.test(input,expect,463))

#     def test_type_mismatch_in_arraycell_1(self):
#         input = Program([
#                     VarDecl(Id("a"),ArrayType(IntLiteral(1),IntLiteral(5),IntType())),
#                     VarDecl(Id("b"),FloatType()),
#                     FuncDecl(Id("main"),[],[], 
#                         [ CallStmt(Id("putIntLn"), [ArrayCell(Id("a"), Id("b"))]) ])
#                     ])
#         expect = "Type Mismatch In Expression: ArrayCell(Id(a),Id(b))"
#         self.assertTrue(TestChecker.test(input,expect,464))

#     def test_type_of_arraycell_1(self):
#         input = Program([
#                     VarDecl(Id("a"),ArrayType(IntLiteral(1),IntLiteral(5),FloatType())),
#                     VarDecl(Id("b"),IntType()),
#                     FuncDecl(Id("main"),[],[], 
#                         [ CallStmt(Id("putIntLn"), [ArrayCell(Id("a"), Id("b"))]) ])
#                     ])
#         expect = "Type Mismatch In Statement: CallStmt(Id(putIntLn),[ArrayCell(Id(a),Id(b))])"
#         self.assertTrue(TestChecker.test(input,expect,465))