# Generated from main/mp/parser/MP.g4 by ANTLR 4.7.1
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys

def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\20")
        buf.write("K\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b")
        buf.write("\t\b\4\t\t\t\4\n\t\n\4\13\t\13\3\2\6\2\30\n\2\r\2\16\2")
        buf.write("\31\3\2\3\2\3\3\3\3\5\3 \n\3\3\4\3\4\3\4\3\4\3\4\3\4\3")
        buf.write("\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6")
        buf.write("\3\6\3\7\3\7\5\79\n\7\3\7\3\7\3\b\3\b\3\b\3\t\3\t\3\t")
        buf.write("\5\tC\n\t\3\t\3\t\3\n\3\n\3\13\3\13\3\13\2\2\f\2\4\6\b")
        buf.write("\n\f\16\20\22\24\2\2\2D\2\27\3\2\2\2\4\37\3\2\2\2\6!\3")
        buf.write("\2\2\2\b(\3\2\2\2\n\61\3\2\2\2\f\66\3\2\2\2\16<\3\2\2")
        buf.write("\2\20?\3\2\2\2\22F\3\2\2\2\24H\3\2\2\2\26\30\5\4\3\2\27")
        buf.write("\26\3\2\2\2\30\31\3\2\2\2\31\27\3\2\2\2\31\32\3\2\2\2")
        buf.write("\32\33\3\2\2\2\33\34\7\2\2\3\34\3\3\2\2\2\35 \5\b\5\2")
        buf.write("\36 \5\6\4\2\37\35\3\2\2\2\37\36\3\2\2\2 \5\3\2\2\2!\"")
        buf.write("\7\t\2\2\"#\7\16\2\2#$\7\4\2\2$%\7\5\2\2%&\7\6\2\2&\'")
        buf.write("\5\f\7\2\'\7\3\2\2\2()\7\n\2\2)*\7\16\2\2*+\7\4\2\2+,")
        buf.write("\7\5\2\2,-\7\7\2\2-.\5\24\13\2./\7\6\2\2/\60\5\f\7\2\60")
        buf.write("\t\3\2\2\2\61\62\7\20\2\2\62\63\7\16\2\2\63\64\7\7\2\2")
        buf.write("\64\65\5\24\13\2\65\13\3\2\2\2\668\7\13\2\2\679\5\16\b")
        buf.write("\28\67\3\2\2\289\3\2\2\29:\3\2\2\2:;\7\f\2\2;\r\3\2\2")
        buf.write("\2<=\5\20\t\2=>\7\6\2\2>\17\3\2\2\2?@\7\16\2\2@B\7\4\2")
        buf.write("\2AC\5\22\n\2BA\3\2\2\2BC\3\2\2\2CD\3\2\2\2DE\7\5\2\2")
        buf.write("E\21\3\2\2\2FG\7\3\2\2G\23\3\2\2\2HI\7\r\2\2I\25\3\2\2")
        buf.write("\2\6\31\378B")
        return buf.getvalue()


class MPParser ( Parser ):

    grammarFileName = "MP.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "<INVALID>", "'('", "')'", "';'", "':'" ]

    symbolicNames = [ "<INVALID>", "INTLIT", "LB", "RB", "SEMI", "COLON", 
                      "WS", "PROCEDURE", "FUNCTION", "BEGIN", "END", "INTTYPE", 
                      "ID", "ERROR_CHAR", "VAR" ]

    RULE_program = 0
    RULE_decl = 1
    RULE_procdecl = 2
    RULE_funcdecl = 3
    RULE_vardecl = 4
    RULE_body = 5
    RULE_stmt = 6
    RULE_funcall = 7
    RULE_exp = 8
    RULE_mtype = 9

    ruleNames =  [ "program", "decl", "procdecl", "funcdecl", "vardecl", 
                   "body", "stmt", "funcall", "exp", "mtype" ]

    EOF = Token.EOF
    INTLIT=1
    LB=2
    RB=3
    SEMI=4
    COLON=5
    WS=6
    PROCEDURE=7
    FUNCTION=8
    BEGIN=9
    END=10
    INTTYPE=11
    ID=12
    ERROR_CHAR=13
    VAR=14

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class ProgramContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def EOF(self):
            return self.getToken(MPParser.EOF, 0)

        def decl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MPParser.DeclContext)
            else:
                return self.getTypedRuleContext(MPParser.DeclContext,i)


        def getRuleIndex(self):
            return MPParser.RULE_program

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitProgram" ):
                return visitor.visitProgram(self)
            else:
                return visitor.visitChildren(self)




    def program(self):

        localctx = MPParser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_program)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 21 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 20
                self.decl()
                self.state = 23 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not (_la==MPParser.PROCEDURE or _la==MPParser.FUNCTION):
                    break

            self.state = 25
            self.match(MPParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class DeclContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def funcdecl(self):
            return self.getTypedRuleContext(MPParser.FuncdeclContext,0)


        def procdecl(self):
            return self.getTypedRuleContext(MPParser.ProcdeclContext,0)


        def getRuleIndex(self):
            return MPParser.RULE_decl

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDecl" ):
                return visitor.visitDecl(self)
            else:
                return visitor.visitChildren(self)




    def decl(self):

        localctx = MPParser.DeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_decl)
        try:
            self.state = 29
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [MPParser.FUNCTION]:
                self.enterOuterAlt(localctx, 1)
                self.state = 27
                self.funcdecl()
                pass
            elif token in [MPParser.PROCEDURE]:
                self.enterOuterAlt(localctx, 2)
                self.state = 28
                self.procdecl()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ProcdeclContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def PROCEDURE(self):
            return self.getToken(MPParser.PROCEDURE, 0)

        def ID(self):
            return self.getToken(MPParser.ID, 0)

        def LB(self):
            return self.getToken(MPParser.LB, 0)

        def RB(self):
            return self.getToken(MPParser.RB, 0)

        def SEMI(self):
            return self.getToken(MPParser.SEMI, 0)

        def body(self):
            return self.getTypedRuleContext(MPParser.BodyContext,0)


        def getRuleIndex(self):
            return MPParser.RULE_procdecl

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitProcdecl" ):
                return visitor.visitProcdecl(self)
            else:
                return visitor.visitChildren(self)




    def procdecl(self):

        localctx = MPParser.ProcdeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_procdecl)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 31
            self.match(MPParser.PROCEDURE)
            self.state = 32
            self.match(MPParser.ID)
            self.state = 33
            self.match(MPParser.LB)
            self.state = 34
            self.match(MPParser.RB)
            self.state = 35
            self.match(MPParser.SEMI)
            self.state = 36
            self.body()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class FuncdeclContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def FUNCTION(self):
            return self.getToken(MPParser.FUNCTION, 0)

        def ID(self):
            return self.getToken(MPParser.ID, 0)

        def LB(self):
            return self.getToken(MPParser.LB, 0)

        def RB(self):
            return self.getToken(MPParser.RB, 0)

        def COLON(self):
            return self.getToken(MPParser.COLON, 0)

        def mtype(self):
            return self.getTypedRuleContext(MPParser.MtypeContext,0)


        def SEMI(self):
            return self.getToken(MPParser.SEMI, 0)

        def body(self):
            return self.getTypedRuleContext(MPParser.BodyContext,0)


        def getRuleIndex(self):
            return MPParser.RULE_funcdecl

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFuncdecl" ):
                return visitor.visitFuncdecl(self)
            else:
                return visitor.visitChildren(self)




    def funcdecl(self):

        localctx = MPParser.FuncdeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_funcdecl)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 38
            self.match(MPParser.FUNCTION)
            self.state = 39
            self.match(MPParser.ID)
            self.state = 40
            self.match(MPParser.LB)
            self.state = 41
            self.match(MPParser.RB)
            self.state = 42
            self.match(MPParser.COLON)
            self.state = 43
            self.mtype()
            self.state = 44
            self.match(MPParser.SEMI)
            self.state = 45
            self.body()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class VardeclContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def VAR(self):
            return self.getToken(MPParser.VAR, 0)

        def ID(self):
            return self.getToken(MPParser.ID, 0)

        def COLON(self):
            return self.getToken(MPParser.COLON, 0)

        def mtype(self):
            return self.getTypedRuleContext(MPParser.MtypeContext,0)


        def getRuleIndex(self):
            return MPParser.RULE_vardecl

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVardecl" ):
                return visitor.visitVardecl(self)
            else:
                return visitor.visitChildren(self)




    def vardecl(self):

        localctx = MPParser.VardeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_vardecl)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 47
            self.match(MPParser.VAR)
            self.state = 48
            self.match(MPParser.ID)
            self.state = 49
            self.match(MPParser.COLON)
            self.state = 50
            self.mtype()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class BodyContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def BEGIN(self):
            return self.getToken(MPParser.BEGIN, 0)

        def END(self):
            return self.getToken(MPParser.END, 0)

        def stmt(self):
            return self.getTypedRuleContext(MPParser.StmtContext,0)


        def getRuleIndex(self):
            return MPParser.RULE_body

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBody" ):
                return visitor.visitBody(self)
            else:
                return visitor.visitChildren(self)




    def body(self):

        localctx = MPParser.BodyContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_body)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 52
            self.match(MPParser.BEGIN)
            self.state = 54
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==MPParser.ID:
                self.state = 53
                self.stmt()


            self.state = 56
            self.match(MPParser.END)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class StmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def funcall(self):
            return self.getTypedRuleContext(MPParser.FuncallContext,0)


        def SEMI(self):
            return self.getToken(MPParser.SEMI, 0)

        def getRuleIndex(self):
            return MPParser.RULE_stmt

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStmt" ):
                return visitor.visitStmt(self)
            else:
                return visitor.visitChildren(self)




    def stmt(self):

        localctx = MPParser.StmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_stmt)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 58
            self.funcall()
            self.state = 59
            self.match(MPParser.SEMI)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class FuncallContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(MPParser.ID, 0)

        def LB(self):
            return self.getToken(MPParser.LB, 0)

        def RB(self):
            return self.getToken(MPParser.RB, 0)

        def exp(self):
            return self.getTypedRuleContext(MPParser.ExpContext,0)


        def getRuleIndex(self):
            return MPParser.RULE_funcall

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFuncall" ):
                return visitor.visitFuncall(self)
            else:
                return visitor.visitChildren(self)




    def funcall(self):

        localctx = MPParser.FuncallContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_funcall)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 61
            self.match(MPParser.ID)
            self.state = 62
            self.match(MPParser.LB)
            self.state = 64
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==MPParser.INTLIT:
                self.state = 63
                self.exp()


            self.state = 66
            self.match(MPParser.RB)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ExpContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INTLIT(self):
            return self.getToken(MPParser.INTLIT, 0)

        def getRuleIndex(self):
            return MPParser.RULE_exp

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExp" ):
                return visitor.visitExp(self)
            else:
                return visitor.visitChildren(self)




    def exp(self):

        localctx = MPParser.ExpContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_exp)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 68
            self.match(MPParser.INTLIT)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class MtypeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INTTYPE(self):
            return self.getToken(MPParser.INTTYPE, 0)

        def getRuleIndex(self):
            return MPParser.RULE_mtype

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMtype" ):
                return visitor.visitMtype(self)
            else:
                return visitor.visitChildren(self)




    def mtype(self):

        localctx = MPParser.MtypeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_mtype)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 70
            self.match(MPParser.INTTYPE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





