
"""
 * @author nhphung
"""
from AST import * 
from Visitor import *
from Utils import Utils
from StaticError import *
from functools import reduce


class MType:
    #partype : list cac type nen doi string
    def __init__(self,partype,rettype):
        self.partype = partype
        self.rettype = rettype
    def __str__(self):
        return 'MType([' + ','.join(str(i) for i in self.partype) + ']' + ',' + str(self.rettype) + ')'

class Symbol:
    def __init__(self,name,mtype,value = None):
        self.name = name
        self.mtype = mtype
        self.value = value
    def __str__(self):
        return 'Symbol(' + self.name + ',' + str(self.mtype) + ')'




        

class StaticChecker(BaseVisitor,Utils):

    global_envi = [Symbol("getInt",MType([],IntType())),
                   Symbol("putInt",MType([IntType()],VoidType())),
                   Symbol("getFloat",MType([],FloatType())),
                   Symbol("putFloat",MType([FloatType()],VoidType())),
                   Symbol("putFloatLn",MType([FloatType()],VoidType())),
                   Symbol("putBool",MType([BoolType()],VoidType())),
                   Symbol("putBoolLn",MType([BoolType()],VoidType())),
                   Symbol("putString",MType([StringType()],VoidType())),
                   Symbol("putStringLn",MType([StringType()],VoidType())),
                   Symbol("putLn",MType([],VoidType())),
                   Symbol("putIntLn",MType([IntType()],VoidType()))]
            
    
    def __init__(self,ast):
        self.ast = ast

    def checkRedeclared(self,sym,kind,env):
        if self.lookup(sym.name,env,lambda x: x.name):
            raise Redeclared(kind,sym.name)
        else:
            return sym

   
    def check(self):
        return self.visit(self.ast,StaticChecker.global_envi)

    def visitProgram(self,ast, c):
        for x in ast.decl :
            if type(x) is FuncDecl :
                kind = Procedure() if type(x.returnType) is VoidType else Function()
                res = self.checkRedeclared(Symbol(x.name.name,MType([i.varType for i in x.param],x.returnType)),kind,c)
                c.append(res)
            else:
                res = self.checkRedeclared(Symbol(x.variable.name,x.varType),Variable(),c)
                c.append(res)

        for x in ast.decl:
            self.visit(x,c)
        return c
        # return reduce(lambda x, y: [self.visit(y, x+c)] + x, ast.decl, []) # array x cong don sau moi lan visit

    def visitDecl(self, ast, c):
        pass


    def visitFuncDecl(self,ast, c): # thieu redecl trong para
        print ("gia tri cua env :")
        for x in c :
            print(x)
        # # vistit(True) la function , False la Procedure
        # kind = Procedure() if type(ast.returnType) is VoidType else Function()
        # #check redecl name fucn/pro
        # global_var = self.checkRedeclared(Symbol(ast.name.name,MType([i.varType for i in ast.param],ast.returnType)),kind,c)
        #chech redecl para
        param = reduce(lambda x,y: [self.checkRedeclared(Symbol(y.variable.name,y.varType),Parameter(),x)] + x ,ast.param,[]) 
        #check local redecl 
        para_local = reduce(lambda x,y: [self.checkRedeclared(Symbol(y.variable.name,y.varType),Variable(),x)] + x ,ast.local,param)
        env_body = [Symbol(ast.name.name,MType([i.varType for i in ast.param],ast.returnType))] + para_local + c

        tmp = list(map(lambda x: self.visit(x,env_body),ast.body))


    # def visitVarDecl(self,ast,c):
        
    #     return self.checkRedeclared(Symbol(ast.variable.name,ast.varType),Variable(),c)
    

    def visitCallStmt(self, ast, c): 
        
        res = self.lookup(ast.method.name,c,lambda x: x.name)
        if res is None or not type(res.mtype) is MType or not type(res.mtype.rettype) is VoidType:
            raise Undeclared(Procedure(),ast.method.name)
        elif len(res.mtype.partype) != len([self.visit(x,c) for x in ast.param]) or True in [type(a) != type(b) for a,b in zip([self.visit(x,c) for x in ast.param], res.mtype.partype)]:
        
            raise TypeMismatchInStatement(ast)    
                
        else:
            return res.mtype.rettype


    def visitIntLiteral(self,ast, c): 
        return IntType()

    def visitFloatLiteral(self,ast,c):
        return FloatType()

    def visitStringLiteral(self,ast,c):
        return StringType()

    def visitBooleanLiteral(self,ast,c):
        return BoolType()

    

    def visitId(self, ast, c):
        res = self.lookup(ast.name, c, lambda x: x.name)
        if res is None:
            raise Undeclared(Identifier(), ast.name)
        else:
            return res.mtype

    def visitBinaryOp(self,ast,c): # thieu typemisstake in expression
        lefttype = self.visit(ast.left,c)
        righttype = self.visit(ast.right,c)

        if ast.op in ["=","<>","<","<=",">",">="] :
            if type(lefttype) in [FloatType,IntType] and type(righttype) in [FloatType,IntType]: 
                return BoolType()
            else:
                raise TypeMismatchInExpression(ast)
        elif ast.op in ["+","-","*"] :
            if type(lefttype) in [FloatType,IntType] and type(righttype) in [FloatType,IntType]: 
               

                if(type(lefttype) == FloatType) or (type(righttype) == FloatType):
                    return FloatType()
                else :
                    return lefttype
            else :
                raise TypeMismatchInExpression(ast)

        elif ast.op == "/" :
            if type(lefttype) in [FloatType,IntType] and type(righttype) in [FloatType,IntType]: 
                return FloatType()
            else :
                raise TypeMismatchInExpression(ast)

        elif ast.op in ["div","mod"]:
            if(type(lefttype) == IntType) and (type(righttype) == IntType) :
                return IntType()
            else :
                raise TypeMismatchInExpression(ast)

        elif ast.op in ["and","andthen","or","orelse"]:
            if (type(lefttype) == BoolType) and (type(righttype) == BoolType) :
                return BoolType()
            else :
                raise TypeMismatchInExpression(ast)

    def visitUnaryOp(self,ast,c):
        exp_type = self.visit(ast.body,c)
        if (ast.op == "-") :
            if type(exp_type) == FloatType or type(exp_type) == IntType:
                return exp_type
            else :
                raise TypeMismatchInExpression(ast)
        elif (ast.op == "not"):
            if type(exp_type) == BoolType:
                return BoolType()
            else:
                raise TypeMismatchInExpression(ast)

    def visitCallExpr(self,ast,c):
        res = self.lookup(ast.method.name,c,lambda x: x.name)
        if res is None or not type(res.mtype) is MType or type(res.mtype.rettype) is VoidType :
            raise Undeclared(Function(),ast.method.name)
        else:
            return res.mtype.rettype
    
    def visitIf(self,ast,c):
        expr = self.visit(ast.expr,c)
        if(type(expr) != BoolType) :
            raise TypeMismatchInStatement(ast)
        for x in ast.thenStmt:
            self.visit(x,c)
        if ast.elseStmt :
            for y in ast.elseStmt:
                self.visit(y,c)
        else:
            None

         

    def visitFor(self,ast,c):
        id_var = self.visit(ast.id,c)
        expr1 = self.visit(ast.expr1,c)
        expr2 = self.visit(ast.expr2,c)
        if (type(id_var) is not IntType or type(expr1) is not IntType or type(expr2) is not IntType):
            raise TypeMismatchInStatement(ast)
        for x in ast.loop:
            self.visit(x,c)
        

    def visitWhile(self,ast,c) :
        expr = self.visit(ast.exp,c)
        if(type(expr) is not BoolType):
            raise TypeMismatchInStatement(ast)
        for x in ast.sl :
            self.visit(x,c)

    def visitAssign(self,ast,c) :
        lhs = self.visit(ast.lhs,c)
        rhs = self.visit(ast.exp,c)
       
        if(type(lhs) == type(rhs) and type(lhs) is not StringType) :
            
            return lhs
        elif type(rhs) is IntType and type(lhs) is FloatType :
           
            return lhs
        else :
            raise TypeMismatchInStatement(ast)

    def visitArrayCell(self,ast,c):
        lhs = self.visit(ast.arr,c)
        idx = self.visit(ast.idx,c)
        if(type(idx) is IntType):
            if (type(lhs) is ArrayType) :
                return lhs.eleType
            else :
                raise TypeMismatchInExpression(ast)
        else :
            raise TypeMismatchInExpression(ast)

    def visitReturn(self,ast,c):
        funct = self.lookup(MType,c,lambda x : type(x.mtype))
        funct_ret = funct.mtype.rettype
        if(ast.expr):
            expr = self.visit(ast.expr,c)
            if(type(expr) is ArrayType):
                if(type(funct_ret) is ArrayType):
                    if((funct_ret.upper == expr.upper) and (funct_ret.lower == expr.lower) and (funct_ret.eleType == expr.eleType)):
                        return expr
                    else:
                        raise TypeMismatchInStatement(ast)
            else:
                if(type(expr) != type(funct_ret)):
                    raise TypeMismatchInStatement(ast)
                else:
                    return expr           
        else :
            if(type(funct_ret) is VoidType) :
                return None
            else:
                raise TypeMismatchInStatement(ast)



        



    
        

            

        
        


